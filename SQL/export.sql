CREATE DATABASE  IF NOT EXISTS `artwork` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artwork`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: artwork
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `idquestion` bigint(255) NOT NULL,
  `iddomainanswer` bigint(255) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,2,8,1),(9,2,9,1),(10,3,10,1),(11,3,11,1),(12,3,12,1),(13,3,13,1),(14,3,14,1),(15,3,15,1),(16,4,8,1),(17,4,9,1),(18,5,16,1),(19,5,17,1),(20,5,18,1),(21,6,19,1),(22,6,20,1),(23,6,21,1),(24,6,22,1),(25,6,23,1),(26,6,24,1),(27,7,19,1),(28,7,20,1),(29,7,21,1),(30,7,22,1),(31,7,23,1),(32,7,24,1),(33,8,19,1),(34,8,20,1),(35,8,21,1),(36,8,22,1),(37,8,23,1),(38,8,24,1),(39,9,19,1),(40,9,20,1),(41,9,21,1),(42,9,22,1),(43,9,23,1),(44,9,24,1),(45,10,25,1),(46,10,26,1),(47,10,27,1),(48,11,19,1),(49,11,20,1),(50,11,21,1),(51,11,22,1),(52,11,23,1),(53,11,24,1),(54,12,19,1),(55,12,20,1),(56,12,21,1),(57,12,22,1),(58,12,23,1),(59,12,24,1),(60,13,19,1),(61,13,20,1),(62,13,21,1),(63,13,22,1),(64,13,23,1),(65,13,24,1),(66,14,19,1),(67,14,20,1),(68,14,21,1),(69,14,22,1),(70,14,23,1),(71,14,24,1),(72,15,8,1),(73,15,9,1),(74,16,19,1),(75,16,20,1),(76,16,22,0),(78,16,21,1),(79,16,22,1),(80,16,23,1),(81,16,24,1),(82,17,33,1),(83,17,34,1),(84,17,35,1),(85,16,36,0),(86,17,36,1),(87,17,37,1),(88,17,33,0),(90,18,33,1),(91,18,34,1),(92,18,35,1),(93,16,36,0),(94,18,36,1),(95,18,37,1),(96,19,19,1),(97,20,19,1),(98,20,20,1),(99,20,21,1),(100,20,22,1),(101,20,23,1),(102,16,24,0),(103,16,24,0),(104,20,24,1),(105,21,19,1),(106,21,20,1),(107,21,21,1),(108,21,22,1),(109,21,23,1),(110,21,24,1),(111,23,19,1),(112,23,20,1),(113,23,21,1),(114,23,22,1),(115,23,23,1),(116,23,24,1),(117,25,19,1),(118,25,20,1),(119,25,21,1),(120,25,22,1),(121,25,23,1),(122,25,24,1),(123,26,19,1),(124,26,20,1),(125,26,21,1),(126,26,22,1),(127,26,23,1),(128,26,24,1),(129,28,19,1),(130,28,20,1),(131,28,21,1),(132,28,22,1),(133,28,23,1),(134,28,24,1),(135,29,19,1),(136,29,20,1),(137,29,21,1),(138,29,22,1),(139,29,23,1),(140,29,24,1),(141,30,48,1),(142,30,3,0),(143,30,4,1),(144,30,5,1),(145,30,49,1),(146,30,50,1),(147,30,51,1),(148,30,52,1),(149,32,53,1),(150,32,54,1),(151,32,52,1),(152,33,56,1),(153,33,57,1),(154,33,58,1),(155,34,59,1),(156,34,60,1),(157,34,61,1),(158,34,62,1),(159,34,63,1),(160,35,64,1),(161,35,65,1),(162,35,66,1),(163,35,67,1),(164,35,68,1),(166,36,64,1),(171,36,65,1),(172,36,66,1),(173,36,67,1),(174,36,68,1),(175,38,69,1),(178,38,9,1),(180,40,19,1),(181,40,20,1),(183,40,21,1),(184,40,22,1),(185,40,23,1),(186,40,24,1),(189,41,19,1),(190,41,20,1),(192,41,21,1),(193,41,22,1),(194,41,23,1),(195,41,24,1),(197,42,19,1),(201,42,20,1),(202,42,21,1),(203,42,22,1),(204,42,23,1),(205,42,24,1),(207,44,19,1),(208,44,20,1),(209,44,21,1),(210,44,22,1),(211,44,23,1),(212,44,24,1),(214,46,19,1),(215,46,20,1),(216,46,21,1),(217,46,22,1),(218,46,23,1),(219,46,24,1),(220,47,19,1),(221,47,20,1),(222,47,21,1),(223,47,22,1),(224,47,23,1),(225,47,24,1),(226,48,19,1),(227,48,20,1),(228,48,21,1),(229,48,22,1),(230,48,23,1),(231,48,24,1),(232,49,19,1),(233,49,20,1),(234,49,21,1),(235,49,22,1),(236,49,23,1),(237,49,24,1),(238,50,19,1),(239,50,20,1),(240,50,21,1),(241,50,22,1),(242,50,23,1),(243,50,24,1),(244,52,19,1),(245,52,20,1),(246,52,21,1),(247,52,22,1),(248,52,23,1),(249,52,24,1),(250,53,69,1),(251,53,9,1),(252,54,69,1),(253,54,9,1),(254,55,19,1),(255,55,20,1),(256,55,21,1),(257,55,22,1),(258,55,23,1),(259,55,24,1),(260,56,8,1),(261,56,9,1),(264,67,1,1),(265,67,3,1),(266,67,5,1),(267,67,4,1),(268,67,38,1),(269,67,39,1),(270,67,40,1),(271,68,41,1),(272,68,42,1),(273,68,43,1),(274,68,44,1),(275,69,41,1),(276,69,42,1),(277,69,43,1),(278,69,44,1),(279,70,41,1),(280,70,42,1),(281,70,43,1),(282,70,44,1),(283,71,41,1),(284,71,42,1),(285,71,43,1),(286,71,44,1),(287,72,41,1),(288,72,42,1),(289,72,43,1),(290,72,44,1),(291,73,41,1),(292,73,42,1),(293,73,43,1),(294,73,44,1),(295,74,41,1),(296,74,42,1),(297,74,43,1),(298,74,44,1),(299,75,41,1),(300,75,42,1),(301,75,43,1),(302,75,44,1),(303,76,41,1),(304,76,42,1),(305,76,43,1),(306,76,44,1),(307,77,41,1),(308,77,42,1),(309,77,43,1),(310,77,44,1),(311,78,41,1),(312,78,42,1),(313,78,43,1),(314,78,44,1),(315,79,41,1),(316,79,42,1),(317,79,43,1),(318,79,44,1),(319,80,41,1),(320,80,42,1),(321,80,43,1),(322,80,44,1),(323,81,41,1),(324,81,42,1),(325,81,43,1),(326,81,44,1),(327,83,45,1),(328,83,46,1),(329,83,26,1),(330,83,47,1),(331,84,45,1),(332,84,46,1),(333,84,26,1),(334,84,47,1),(335,86,45,1),(336,86,46,1),(337,86,47,1),(338,86,26,1),(339,87,26,1),(340,87,45,1),(341,87,44,0),(342,87,46,1),(343,87,47,1),(344,88,26,1),(345,88,45,1),(346,88,46,1),(347,88,47,1),(348,89,26,1),(349,89,45,1),(350,89,46,1),(351,89,47,1);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `choices`
--

DROP TABLE IF EXISTS `choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `choices` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `customer` bigint(255) NOT NULL,
  `idanswer` bigint(255) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `choices`
--

LOCK TABLES `choices` WRITE;
/*!40000 ALTER TABLE `choices` DISABLE KEYS */;
INSERT INTO `choices` VALUES (74,1,1,1),(75,1,9,1),(76,1,10,1),(77,1,16,1),(78,1,20,1),(79,1,23,1),(80,1,29,1),(81,1,38,1),(82,1,42,1),(83,1,45,1),(84,1,50,1),(85,1,56,1),(86,1,65,1),(87,1,71,1),(88,1,73,1),(89,2,4,1),(90,2,9,1),(91,2,11,1),(92,2,17,1),(93,2,19,1),(94,2,22,1),(95,2,30,1),(96,2,36,1),(97,2,41,1),(98,2,46,1),(99,2,49,1),(100,2,57,1),(101,2,62,1),(102,2,68,1),(103,2,73,1),(104,3,3,1),(105,3,9,1),(106,3,11,1),(107,3,17,1),(108,3,19,1),(109,3,21,1),(110,3,29,1),(111,3,38,1),(112,3,43,1),(113,3,45,1),(114,3,50,1),(115,3,57,1),(116,3,64,1),(117,3,68,1),(118,3,73,1),(119,4,3,1),(120,4,8,1),(121,4,10,1),(122,4,16,1),(123,4,19,1),(124,4,23,1),(125,4,30,1),(126,4,36,1),(127,4,42,1),(128,4,46,1),(129,4,51,1),(130,4,57,1),(131,4,63,1),(132,4,70,1),(133,4,72,1),(134,5,4,1),(135,5,8,1),(136,5,12,1),(137,5,16,1),(138,5,18,1),(139,5,24,1),(140,5,32,1),(141,5,36,1),(142,5,41,1),(143,5,47,1),(144,5,51,1),(145,5,58,1),(146,5,63,1),(147,5,69,1),(148,5,72,1),(149,6,3,1),(150,6,8,1),(151,6,15,1),(152,6,16,1),(153,6,18,1),(154,6,23,1),(155,6,30,1),(156,6,36,1),(157,6,42,1),(158,6,47,1),(159,6,52,1),(160,6,57,1),(161,6,65,1),(162,6,70,1),(163,6,73,1),(164,7,78,1),(165,7,84,1),(166,7,94,1),(167,7,99,1),(168,7,108,1),(169,7,120,1),(170,7,127,1),(171,7,133,1),(172,7,139,1),(173,8,78,1),(174,8,82,1),(175,8,94,1),(176,8,104,1),(177,8,108,1),(178,8,119,1),(179,8,124,1),(180,8,131,1),(181,8,138,1),(182,9,78,1),(183,9,82,1),(184,9,95,1),(185,9,99,1),(186,9,108,1),(187,9,121,1),(188,9,127,1),(189,9,132,1),(190,9,140,1),(191,10,75,1),(192,10,83,1),(193,10,92,1),(194,10,99,1),(195,10,109,1),(196,10,119,1),(197,10,125,1),(198,10,133,1),(199,10,140,1),(200,11,78,1),(201,11,84,1),(202,11,95,1),(203,11,99,1),(204,11,110,1),(205,11,119,1),(206,11,124,1),(207,11,132,1),(208,11,140,1),(209,12,78,1),(210,12,87,1),(211,12,94,1),(212,12,101,1),(213,12,107,1),(214,12,122,1),(215,12,125,1),(216,12,129,1),(217,12,140,1),(218,13,78,1),(219,13,83,1),(220,13,90,1),(221,13,99,1),(222,13,108,1),(223,13,118,1),(224,13,126,1),(225,13,129,1),(226,13,139,1),(227,14,75,1),(228,14,87,1),(229,14,90,1),(230,14,100,1),(231,14,109,1),(232,14,118,1),(233,14,126,1),(234,14,133,1),(235,14,137,1),(236,15,3,1),(237,15,9,1),(238,15,11,1),(239,15,16,1),(240,15,19,1),(241,15,24,1),(242,15,29,1),(243,15,36,1),(244,15,42,1),(245,15,47,1),(246,15,50,1),(247,15,59,1),(248,15,65,1),(249,15,67,1),(250,15,72,1),(251,16,260,1),(252,16,264,1),(253,16,273,1),(254,16,277,1),(255,16,280,1),(256,16,286,1),(257,16,289,1),(258,16,293,1),(259,16,297,1),(260,16,300,1),(261,16,304,1),(262,16,308,1),(263,16,313,1),(264,16,317,1),(265,16,321,1),(266,16,325,1),(267,16,328,1),(268,16,332,1),(269,16,338,1),(270,16,342,1),(271,16,344,1),(272,16,348,1),(273,17,260,1),(274,17,267,1),(275,17,272,1),(276,17,277,1),(277,17,281,1),(278,17,286,1),(279,17,288,1),(280,17,292,1),(281,17,295,1),(282,17,300,1),(283,17,305,1),(284,17,307,1),(285,17,314,1),(286,17,317,1),(287,17,321,1),(288,17,324,1),(289,17,328,1),(290,17,334,1),(291,17,338,1),(292,17,339,1),(293,17,345,1),(294,17,350,1),(295,18,75,1),(296,18,83,1),(297,18,94,1),(298,18,99,1),(299,18,107,1),(300,18,121,1),(301,18,127,1),(302,18,133,1),(303,18,138,1);
/*!40000 ALTER TABLE `choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domainanswers`
--

DROP TABLE IF EXISTS `domainanswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domainanswers` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `value` double NOT NULL,
  `visibility` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domainanswers`
--

LOCK TABLES `domainanswers` WRITE;
/*!40000 ALTER TABLE `domainanswers` DISABLE KEYS */;
INSERT INTO `domainanswers` VALUES (1,'Stampa specializzata / quotidiani / riviste',0,'1'),(2,'Stampa quotidiani',0,'1'),(3,'Facebook',0,'1'),(4,'Twitter',0,'1'),(5,'Instagram',0,'1'),(6,'Amici e/o conoscenti',0,'1'),(7,'Mailing list',0,'1'),(8,'Si',1,'1'),(9,'No',0,'1'),(10,'Immersiva',6,'1'),(11,'Interattiva',4,'1'),(12,'Coinvolgente',5,'1'),(13,'Confusa',2,'1'),(14,'Povera di materiali',1,'1'),(15,'Poco / per niente interessante',0,'1'),(16,'Soddisfacente',2,'1'),(17,'Poco soddisfacente',1,'1'),(18,'Per nulla soddisfacente',0,'1'),(19,'0',0,'1'),(20,'1',1,'1'),(21,'2',2,'1'),(22,'3',3,'1'),(23,'4',4,'1'),(24,'5',5,'1'),(25,'Troppo costoso',5,'1'),(26,'Basso',1,'1'),(27,'Equo',3,'1'),(31,'Sito ufficiale museo ',0,'1'),(32,'Multisensoriale',3,'1'),(33,'Installazioni interattive',0,'1'),(34,'Guide virtuali',0,'1'),(35,'Multiproiezioni',0,'1'),(36,'Nube ardente',0,'1'),(37,'Il film sull’Eruzione del Vesuvio',0,'1'),(38,'Sito ufficiale internet / motori di ricerca',0,'1'),(39,'Newsletter',0,'1'),(40,'Altro',0,'1'),(41,'Molto',3,'1'),(42,'Abbastanza',2,'1'),(43,'Poco',1,'1'),(44,'Per nulla',0,'1'),(45,'Alto',3,'1'),(46,'Medio',2,'1'),(47,'Non so',0,'1'),(48,'Newspaper',0,'1'),(49,'Official site',0,'1'),(50,'Recommendation from someone',0,'1'),(51,'TV',0,'1'),(52,'Other',0,'1'),(53,'Interest',0,'1'),(54,'Educational',0,'1'),(55,'Other',0,'1'),(56,'30 min',0,'1'),(57,'1 hour',0,'1'),(58,'More than 1 hour ',0,'1'),(59,'3D',0,'1'),(60,'Multi-Projection',0,'1'),(61,'Sensorial Effects',0,'1'),(62,'Virtual guides',0,'1'),(63,'5D',0,'1'),(64,'Strongly Agree',4,'1'),(65,'Agree',3,'1'),(66,'Neither/ Unsure',2,'1'),(67,'Disagree',1,'1'),(68,'Strongly Disagree',0,'1'),(69,'Yes',0,'1');
/*!40000 ALTER TABLE `domainanswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domainquestions`
--

DROP TABLE IF EXISTS `domainquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domainquestions` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domainquestions`
--

LOCK TABLES `domainquestions` WRITE;
/*!40000 ALTER TABLE `domainquestions` DISABLE KEYS */;
INSERT INTO `domainquestions` VALUES (1,'Come ha saputo dell\'esistenza del museo?',1),(2,'E\' la prima volta che visita questo Museo?',1),(3,'Indichi quale tra le seguenti definizioni rispecchia la sua esperienza di visita:',1),(4,'Le tecnologie presenti nel museo sono utili alla sua comprensione delle antiche città di Pompei ed Ercolano?',1),(5,'Come giudica l\'allestimento museale?',1),(6,'Cunicoli di scavo / Fogscreen',1),(7,'Edifici pubblici / Teatro / Terme / Faro / Area sacra',1),(8,'Domus / Casa del Poeta Tragico / Casa del Citarista / Casa del Fauno / Casa del Labirinto ',1),(9,'Villa dei Papiri',1),(10,'Come giudica il prezzo del ticket?',1),(11,'Dopo questa esperienza ritornerebbe / consiglierebbe una visita?',1),(12,'Vuoi essere aggiornato sulle iniziative del museo?',0),(13,'Suggerimenti e proposte',0),(14,'Shop',1),(15,'Bar',1),(16,'Front office',1),(17,'Servizi / Toilettes',1),(18,'Ti sei divertito durante la visita al Museo?',1),(19,'Quale è la cosa che ti è piaciuta di più? ',1),(20,'E quella che ti è piaciuta di meno? ',1),(21,'Questo Museo lo ritieni adatto a te? ',1),(22,'Sei contento della guida che ti ha accompagnato durante il percorso? ',1),(23,'Gli argomenti trattati durante il percorso sono stati chiari?',1),(24,'Come giudica l\'esperienza nella sala 5D con il film sull\'Eruzione del Vesuvio?',1),(25,'La visita al museo ha soddisfatto le aspettative del gruppo?',1),(26,'Accoglienza da parte del personale',1),(27,'Prezzo',1),(28,'Durata della visita guidata',1),(29,'Qualità dei contenuti multimediali',1),(30,'Qualità dei supporti multimediali',1),(31,'Competenza / Chiarezza delle guide',1),(32,'Servizio di prenotazione',1),(33,'Indicazioni per raggiungere il museo',1),(34,'Qualità degli ambienti',1),(35,'Accessibilità da parte di persone con difficoltà motorie',1),(36,'Qualità dei punti di ristoro',1),(37,'Qualità / pulizia dei servizi igienici',1),(38,'Qual è il livello di gradimento in relazione all\'esperienza del nuovo all\'estimento MAV 4.0 5 Sense sul gruppo?',1),(39,'Cunicolo Borbonico',1),(40,'Edifici  pubblici',1),(41,'Domus',1),(42,'How did you hear about the MAV?',1),(43,'Which is the reason for the visiting the museum?',1),(44,'Approximately how long have you spent in the museum?',1),(45,'What, if anything, do you find particularly attractive or appealing about the museum?',1),(46,'The price is reasonable',1),(47,'MAV staff members were helpful and friendly',1),(48,'Did you visit Ercolano\'s archaeological site?',1),(49,'Rate your experience in museum\'s section',1),(50,'Would recommend visiting friends and relatives?',1),(51,'Would you like to be informed about the museum\'s activity?',1),(52,'Rate your  experience in MAV?',1),(53,'Bourbon Tunnel',1),(54,'Public Buildings',1),(55,'Villa of the Papyri',1),(56,'The movie “The eruption of the Vesuvius”',1),(57,'3-D recreation',1),(58,'Multi-projection',1),(59,'Virtual guides',1),(60,'Sensory effects ',1),(61,'5D Room',1),(62,'Ritorneresti volentieri al Museo?',1),(63,'Consiglieresti il Museo ad un amico?',1);
/*!40000 ALTER TABLE `domainquestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES (1,'gianni@libero.it',1),(2,'gino@gmail.com',1),(3,'prova@libero.it',1),(4,'liberato@gmail.it',1),(6,'pippo@tim.it',1);
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupquestions`
--

DROP TABLE IF EXISTS `groupquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupquestions` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `idsurvey` bigint(255) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupquestions`
--

LOCK TABLES `groupquestions` WRITE;
/*!40000 ALTER TABLE `groupquestions` DISABLE KEYS */;
INSERT INTO `groupquestions` VALUES (9,'',1,1),(10,'',1,1),(11,'',1,1),(12,'',1,1),(13,'',1,1),(14,'Come giudica le varie sezioni del museo? (esprimere un valore da 0 a 5):',1,1),(15,'',1,1),(16,'Come giudica i seguenti servizi? (esprimere un valore da 0 a 5):',1,1),(17,'',1,1),(20,'',2,1),(21,'',2,1),(22,'',2,1),(23,'',2,1),(24,'',2,1),(25,'',2,1),(26,'',2,1),(27,'',2,1),(28,'',2,1),(29,'',3,1),(30,'',3,1),(31,'',3,0),(32,'',3,1),(33,'',3,1),(34,'',3,1),(35,'',3,1),(36,'',3,1),(37,'Rate your experience in museum\'s section',3,1),(38,'Rate museum\'s  technology ',3,1),(39,'',3,1),(40,'',3,1),(41,'',3,1),(42,'',4,1),(43,'',4,1),(44,'',4,1),(45,'',4,1),(46,'',4,0),(47,'',4,0),(48,'Indichi il livello di apprezzamento dei seguenti punti:',4,1),(49,'',4,1),(50,'Quale è il livello di apprezzamento delle aree del museo?',4,1),(51,'',4,1);
/*!40000 ALTER TABLE `groupquestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joinsurveys`
--

DROP TABLE IF EXISTS `joinsurveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joinsurveys` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `jkey` varchar(45) NOT NULL,
  `idsurvey` bigint(255) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joinsurveys`
--

LOCK TABLES `joinsurveys` WRITE;
/*!40000 ALTER TABLE `joinsurveys` DISABLE KEYS */;
INSERT INTO `joinsurveys` VALUES (1,'Adulti',1,1),(2,'Ragazzi',2,1),(3,'Gruppi',4,1),(4,'Inglese',3,1),(5,'Francese',0,1),(6,'Tedesco',0,1),(7,'Spagnolo',0,1);
/*!40000 ALTER TABLE `joinsurveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `level` varchar(45) NOT NULL,
  `iduser` bigint(255) NOT NULL,
  `enable` tinyint(4) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'admin0001','admin','Admin',1,1,1),(2,'user0001','user','User',2,1,1);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `idgroupquestion` bigint(255) NOT NULL,
  `iddomainquestion` bigint(255) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,9,1,1),(2,10,2,1),(3,11,3,1),(4,12,4,1),(5,13,5,1),(6,14,6,1),(7,14,7,1),(8,14,8,1),(9,14,9,1),(10,15,10,1),(11,16,16,1),(12,16,17,1),(13,16,14,1),(14,16,15,1),(15,17,11,1),(16,20,18,1),(17,21,19,1),(18,22,20,1),(20,23,21,1),(21,24,22,1),(22,24,11,0),(23,25,11,0),(24,24,62,0),(25,25,62,1),(26,26,63,1),(27,27,63,0),(28,27,23,1),(29,28,24,1),(30,29,42,1),(31,29,43,0),(32,30,43,1),(33,32,44,1),(34,33,45,1),(35,34,46,1),(36,35,47,1),(38,36,48,1),(39,37,49,0),(40,37,53,1),(41,37,54,1),(42,37,41,1),(44,37,55,1),(46,37,56,1),(47,38,57,1),(48,38,58,1),(49,38,59,1),(50,38,60,1),(51,38,11,0),(52,38,61,1),(53,39,50,1),(54,40,51,1),(55,41,52,1),(56,42,2,1),(67,43,1,1),(68,44,25,1),(69,45,11,1),(70,48,26,1),(71,48,27,1),(72,48,28,1),(73,48,29,1),(74,48,30,1),(75,48,31,1),(76,48,32,1),(77,48,33,1),(78,48,34,1),(79,48,35,1),(80,48,36,1),(81,48,37,1),(83,49,38,1),(84,50,39,1),(86,50,40,1),(87,50,41,1),(88,50,9,1),(89,51,24,1);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggests`
--

DROP TABLE IF EXISTS `suggests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggests` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `suggest` varchar(500) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggests`
--

LOCK TABLES `suggests` WRITE;
/*!40000 ALTER TABLE `suggests` DISABLE KEYS */;
INSERT INTO `suggests` VALUES (2,'Orario di apertura più flessibile',1),(3,'Possibilità di parcheggio',1),(4,'...suggerimento',1);
/*!40000 ALTER TABLE `suggests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `typology` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` VALUES (1,'Visitatore Adulto 1','Adulti','Campo destinato ad accogliere eventuali commenti e descrizione dell\'uso del questionario. Campo visibile solo all\'amministratore del sistema.',1),(2,'Questionario Ragazzi 1','Ragazzi','Campo destinato ad accogliere eventuali commenti e descrizione dell\'uso del questionario. Campo visibile solo all\'amministratore del sistema.',1),(3,'Questionario Inglese 1','Inglese','Campo destinato ad accogliere eventuali commenti e descrizione dell\'uso del questionario. Campo visibile solo all\'amministratore del sistema.',1),(4,'Questionario Gruppi 1','Gruppi','Campo destinato ad accogliere eventuali commenti e descrizione dell\'uso del questionario. Campo visibile solo all\'amministratore del sistema.',1);
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `cap` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Esposito','Vincenzo','Via portale 1','95028','Roma','Dirigente',1),(2,'Rossi','Antonio','Via napoli','80039','Milano','Guest',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-14 16:39:35
