								Artwork
Deriva dall'inglese che significa "Opera d'arte".

Gruppo GANVIM:

Componenti: Giovanni d'Alise, Antonio Rodolfo Napolitano, Nicola Berardino, Vincenzo Comentale, Monica Di Franco.

							Breve Descrizione
			
Artwork � una Web Application per la gestione di dati statistici provenienti dall'elaborazione di questionari sottoposti ai visitatori al termine della visita. Attraverso il questionario � possibile conoscere il livello di gradimento in merito alle tematiche proposte.
			
						Struttura del progetto
			
Il software � diviso in tre moduli:

1) Il primo modulo permette di gestire  l'anagrafica degli utenti che hanno l'accesso all'applicativo.

2) Il secondo modulo riguarda la gestione dei questionari divisi per tipologie di visitatori individuati : visitatore occasionale adulto, bambini, gruppi organizzati e stranieri.

3) Il terzo modulo gestisce le statistiche. Medianti una serie di funzioni i dati presenti nel database vengono elaborati per acquisire le seguenti informazioni: la provenienza, il grado di istruzione, le motivazioni della visita, le abitudini di frequentazione, il grado di interesse in relazione alle aree espositive.

