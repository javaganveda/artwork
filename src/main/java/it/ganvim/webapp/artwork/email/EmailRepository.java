package it.ganvim.webapp.artwork.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class EmailRepository implements Repository<Long,Email> {

	DBRepository dbr = new DBRepository();

	public int add(Email entity) {

		Email email = entity;

		int res = 0;

		List<Email> emails = new ArrayList<Email>();

		String condition = "email = '" + email.getEmail() + "'"; 

		emails = this.getbyFilter(condition);

		if (emails.size() > 0) {
			return 1;
		}

		else {

			email.setEmail(email.getEmail().replace ("'", "''"));

			String sql = "INSERT INTO emails (email, visibility)  VALUES "
					+ "('" + email.getEmail() + "', true" +")"; 

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<Email> getAll() {

		List<Email> emails = new ArrayList <Email> ();

		String sql = "SELECT * FROM emails as e WHERE e.visibility = true";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Email email = new Email();

			email.setId((Long) result.get("id"));
			email.setEmail((String) result.get("email"));
			email.setVisibility(true);
			
			emails.add(email);
		}
		return emails;	
	}

	@Override
	public int update(Email entity) {

		Email email = entity;

		int res = 0;

		email.setEmail(email.getEmail().replace ("'", "''"));

		String sql = "UPDATE emails e  SET email = '" + email.getEmail() +
				"' WHERE id = " +  email.getId();

		res = dbr.update(sql);

		return res;
	}

	@Override
	public Email getbyId(Long id) {

		Email email = new Email();

		String sql = String.format("SELECT * FROM emails as e WHERE e.visibility=true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			email.setId((Long) result.get("id"));
			email.setEmail((String) result.get("email"));
			email.setVisibility(true);
		}
		return email;	
	}

	@Override
	public List<Email> getbyColumn(String column, String value) {

		List<Email> emails = new ArrayList <Email> ();

		String sql = "SELECT * FROM emails as e WHERE  e.visibility=true AND e."
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Email email = new Email();

			email.setId((Long) result.get("id"));
			email.setEmail((String) result.get("email"));
			email.setVisibility(true);

			emails.add(email);
		}
		return emails;
	}

	public int delete(Email entity) {

		Email email = entity;

		int res = 0;

		String sql = String.format("UPDATE emails e SET e.visibility = false WHERE id = %d",
				email.getId());

		res = dbr.delete(sql);

		return res;
	}


	public List<Email> getbyFilter(String condition) {

		List<Email> emails = new ArrayList <Email> ();

		String sql = "SELECT * FROM emails as e WHERE  e.visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Email email = new Email();

			email.setId((Long) result.get("id"));
			email.setEmail((String) result.get("email"));
			email.setVisibility(true);

			emails.add(email);
		}
		return emails;
	}

	@Override
	public List<Email> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
