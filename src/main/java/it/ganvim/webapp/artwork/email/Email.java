package it.ganvim.webapp.artwork.email;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class Email extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String email;

	public Email () {
		
		this.setEmail ("");
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
