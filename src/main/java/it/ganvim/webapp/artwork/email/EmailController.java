package it.ganvim.webapp.artwork.email;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class EmailController extends Controller<Long, Email> {

	private static final long serialVersionUID = 1L;

	private List<Email> emails;

	private String newemail = "";

	@Inject
	private Repository<Long, Email> emailRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.setEmails(new ArrayList<Email>());
		this.list();
	}

	public void list() {
		this.setEmails(emailRepository.getAll());
	}

	public void delete(Long idEmail) {
		
		int res = 0;

		Email email = new Email();
		
		email.setId(idEmail);
		
		res = emailRepository.delete(email);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == -1) {					
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}

		// Redirigere la pagina
		this.redirect("email.xhtml");
	}
	
	public void update(Long idEmail, String mail) {

		int res = 0;

		Email email = new Email();
		
		email.setId(idEmail);
		
		email.setEmail(mail);
		
		if(mail.contains("@") && mail.contains(".")) {

			res = emailRepository.update(email);

			if(res == 0) {			
				this.getSd().setMessage("Modifica avvenuta correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == 1) {					
				this.getSd().setMessage("Email gi� presente in archivio");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}

			if(res == -1) {				
				this.getSd().setMessage("Errore nella modifica");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}

		}
		else {
			//errore E-mail
			this.getSd().setMessage("Formato E-Mail non valido");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
		}
		
		// Redirigere la pagina
		this.redirect("email.xhtml");
	}
	
	public void add(String newmail) {

		int res = 0;

		Email email = new Email();

		email.setEmail(newmail);

		if(newmail.equals("")) {
			this.getSd().setMessage("Campo E-Mail vuoto");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			// Redirigere la pagina
			this.redirect("email.xhtml");
		}
		else
		{
			
			if(newmail.contains("@") && newmail.contains(".")) {

				res = emailRepository.add(email);

				if(res == 0) {	
					this.getSd().setMessage("Inserimento avvenuto correttamente");
					this.getSd().setMessSeverity(SingletonDate.MESSOK);
				}

				if(res == 1) {			
					this.getSd().setMessage("Email gi� presente in archivio");
					this.getSd().setMessSeverity(SingletonDate.MESSWORN);
				}

				if(res == -1) {		
					this.getSd().setMessage("Errore nell'inserimento");
					this.getSd().setMessSeverity(SingletonDate.MESSERROR);
				}	

			}
			else {
				//errore E-mail
				this.getSd().setMessage("Formato E-Mail non valido");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}
			// Redirigere la pagina
			this.redirect("email.xhtml");	
		}
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public String getNewemail() {
		return newemail;
	}

	public void setNewemail(String newemail) {
		this.newemail = newemail;
	}	
}