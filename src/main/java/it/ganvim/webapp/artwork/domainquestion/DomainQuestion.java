package it.ganvim.webapp.artwork.domainquestion;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class DomainQuestion extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String name;

	public DomainQuestion () {

		this.setName("");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
