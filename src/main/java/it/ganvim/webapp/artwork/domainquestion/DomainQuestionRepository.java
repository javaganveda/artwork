package it.ganvim.webapp.artwork.domainquestion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class DomainQuestionRepository implements Repository<Long,DomainQuestion> {

	DBRepository dbr = new DBRepository();

	public int add(DomainQuestion entity) {

		DomainQuestion domainQuestion = entity;

		int res = 0;

		List<DomainQuestion> domainQuestions = new ArrayList<DomainQuestion>();

		String condition = "name = '" + domainQuestion.getName() + "' ";

		domainQuestions = this.getbyFilter(condition);

		if (domainQuestions.size() > 0) {
			return 1;
		}
		else {

			domainQuestion.setName  (domainQuestion.getName().replace ("'", "''"));	

			String sql = String.format("INSERT INTO domainquestions (name, visibility) VALUES "
					+ "('%s', %b)", domainQuestion.getName(), true);

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<DomainQuestion> getAll() {

		List<DomainQuestion> domainQuestions = new ArrayList <DomainQuestion> ();

		String sql = String.format("SELECT * FROM domainquestions as d WHERE d.visibility=true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainQuestion domainquestion = new DomainQuestion();

			domainquestion.setId((Long) result.get("id"));
			domainquestion.setName((String) result.get("name"));
			domainquestion.setVisibility(true);

			domainQuestions.add(domainquestion);
		}
		return domainQuestions;	
	}

	@Override
	public int update(DomainQuestion entity) {

		DomainQuestion domainquestion = entity;

		int res = 0;

		domainquestion.setName (domainquestion.getName().replace ("'", "''"));

		String sql = String.format("UPDATE domainquestions d  SET name = '%s',"
				+ " visibility = %b WHERE d.id = %d", domainquestion.getName(),
				domainquestion.isVisible(), domainquestion.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public DomainQuestion getbyId(Long id) {

		DomainQuestion domainquestion = new DomainQuestion();

		String sql = String.format("SELECT * FROM domainquestions as d WHERE d.visibility=true"
				+ " AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			domainquestion.setId((Long) result.get("id"));
			domainquestion.setName((String) result.get("name"));
			domainquestion.setVisibility(true);
		}
		return domainquestion;	
	}

	@Override
	public List<DomainQuestion> getbyColumn(String column, String value) {

		List<DomainQuestion> domainquestions = new ArrayList <DomainQuestion> ();

		String sql = "SELECT * FROM domainquestions as d WHERE  d.visibility=true AND"
				+ " d." + column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainQuestion domainquestion = new DomainQuestion();

			domainquestion.setId((Long) result.get("id"));
			domainquestion.setName((String) result.get("name"));
			domainquestion.setVisibility(true);

			domainquestions.add(domainquestion);
		}
		return domainquestions;
	}

	@Override
	public int delete(DomainQuestion entity) {

		DomainQuestion domainquestion = entity;

		int res = 0;

		String sql = String.format("UPDATE domainquestions d SET d.visibility = false "
				+ "WHERE id = %d", domainquestion.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<DomainQuestion> getbyFilter(String condition) {

		List<DomainQuestion> domainquestions = new ArrayList <DomainQuestion> ();

		String sql = "SELECT * FROM domainquestions as d WHERE  d.visibility=true "
				+ "AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainQuestion domainquestion = new DomainQuestion();

			domainquestion.setId((Long) result.get("id"));
			domainquestion.setName((String) result.get("name"));
			domainquestion.setVisibility(true);

			domainquestions.add(domainquestion);
		}
		return domainquestions;
	}

	@Override
	public List<DomainQuestion> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isModified(Long id) {
		
		boolean modified = false;

		String sql = String.format("SELECT * FROM  questions AS q,"
				+ " answers AS a, choices AS c " + 
				"WHERE  q.id = a.idquestion AND"
				+ " a.id = c.idanswer AND q.iddomainquestion = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		if(results.size() > 0) {
			modified = false;
		}
		else { 
			modified = true;
			
		}

		return modified;	
	}
}
