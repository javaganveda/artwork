package it.ganvim.webapp.artwork.domainquestion;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class DomainQuestionController extends Controller<Long, DomainQuestion> {

	private static final long serialVersionUID = 1L;

	private List<DomainQuestion> domainQuestions;
	
	private String newname = "";

	@Inject
	private Repository<Long, DomainQuestion> domainQuestionRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.domainQuestions= new ArrayList<DomainQuestion>();
		this.list();
	}

	public void list() {
		this.setDomainQuestions(domainQuestionRepository.getAll());
	}

	public void delete(Long idDomainQuestion) {
		
		int res = 0;

		DomainQuestion domainQuestion = new DomainQuestion();
		
		domainQuestion.setId(idDomainQuestion);
		
		res = domainQuestionRepository.delete(domainQuestion);
		
		if(res == 0) {					
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}

		// Redirigere la pagina
		this.redirect("domande.xhtml");
	}
	
	public void update(Long iddomainQuestion, String name) {

		int res = 0;

		DomainQuestion domainQuestion = new DomainQuestion();
		domainQuestion.setId(iddomainQuestion);
		domainQuestion.setName(name);

		res = domainQuestionRepository.update(domainQuestion);

		if(res == 0) {				
			this.getSd().setMessage("Modifica avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == 1) {					
			this.getSd().setMessage("Doamnda gi� presente in archivio");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella modifica");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}
		
		// Redirigere la pagina
		this.redirect("domande.xhtml");
	}
	
	public void add(String newname) {

		int res = 0;

		DomainQuestion domainQuestion = new DomainQuestion();

		if(newname.equals("")) {
			this.getSd().setMessage("Campo nuova domanda vuoto");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			// Redirigere la pagina
			this.redirect("domande.xhtml");
		}
		else
		{
			domainQuestion.setName(newname);

			res = domainQuestionRepository.add(domainQuestion);

			if(res == 0) {	
				this.getSd().setMessage("Inserimento avvenuto correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == 1) {			
				this.getSd().setMessage("Domanda gi� presente in archivio");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}

			if(res == -1) {			
				this.getSd().setMessage("Errore nell'inserimento");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}	

			// Redirigere la pagina
			this.redirect("domande.xhtml");
		}
	}
	
	public boolean modified(Long iddomainQuestion) {

		boolean res = domainQuestionRepository.isModified(iddomainQuestion);

		return res;
	}

	public List<DomainQuestion> getDomainQuestions() {
		return domainQuestions;
	}

	public void setDomainQuestions(List<DomainQuestion> domainQuestions) {
		this.domainQuestions = domainQuestions;
	}

	public String getNewname() {
		return newname;
	}

	public void setNewname(String newname) {
		this.newname = newname;
	}	
}