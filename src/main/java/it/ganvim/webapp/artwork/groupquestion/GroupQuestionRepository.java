package it.ganvim.webapp.artwork.groupquestion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class GroupQuestionRepository implements Repository<Long,GroupQuestion> {

	DBRepository dbr = new DBRepository();

	public int add(GroupQuestion entity) {

		GroupQuestion gquestion = entity;

		int res = 0;

		List<GroupQuestion> gquestions = new ArrayList<GroupQuestion>();

		gquestion.setName  (gquestion.getName().replace ("'", "''"));

		String condition = " name = '" + gquestion.getName() + "' " ;

		gquestions = this.getbyFilter(condition);

		if (gquestions.size() > 0 && !gquestion.getName().equals("")) {
			return 1;
		}
		else {

			gquestion.setName  (gquestion.getName().replace ("'", "''"));

			String sql = String.format("INSERT INTO groupquestions (name, idsurvey, visibility)"
					+ " VALUES ('%s', %d, %b)", 
					gquestion.getName(), gquestion.getIdsurvey(), true);

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<GroupQuestion> getAll() {

		List<GroupQuestion> gquestions = new ArrayList <GroupQuestion> ();

		String sql = String.format("SELECT * FROM groupquestions as gp WHERE gp.visibility=true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			GroupQuestion gquestion = new GroupQuestion();

			gquestion.setId((Long) result.get("id") );
			gquestion.setName  ((String) result.get("name"));
			gquestion.setIdsurvey((Long) result.get("idsurvey") );
			gquestion.setVisibility(true);

			gquestions.add(gquestion);
		}
		return gquestions;	
	}

	@Override
	public int update(GroupQuestion entity) {

		GroupQuestion gquestion = entity;

		int res = 0;

		gquestion.setName  (gquestion.getName().replace ("'", "''"));

		String sql = String.format("UPDATE groupquestions gp SET name = '%s', idsurvey = %d, "
				+ " visibility = %b WHERE gp.id = %d", gquestion.getName(),
				gquestion.getIdsurvey(), gquestion.isVisible(), gquestion.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public GroupQuestion getbyId(Long id) {

		GroupQuestion gquestion = new GroupQuestion();

		String sql = String.format("SELECT * FROM groupquestions as gp WHERE gp.visibility = true"
				+ " AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			gquestion.setId((Long) result.get("id") );
			gquestion.setName  ((String) result.get("name"));
			gquestion.setIdsurvey((Long) result.get("idsurvey") );
			gquestion.setVisibility(true);
		}
		return gquestion;	
	}

	@Override
	public List<GroupQuestion> getbyColumn(String column, String value) {

		List<GroupQuestion> gquestions = new ArrayList <GroupQuestion> ();

		String sql = "SELECT * FROM groupquestions as gp WHERE  gp.visibility = true AND gp." 
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			GroupQuestion gquestion = new GroupQuestion();

			gquestion.setId((Long) result.get("id") );
			gquestion.setName  ((String) result.get("name"));
			gquestion.setIdsurvey((Long) result.get("idsurvey") );
			gquestion.setVisibility(true);

			gquestions.add(gquestion);
		}
		return gquestions;
	}

	@Override
	public int delete(GroupQuestion entity) {

		GroupQuestion gquestion = entity;

		int res = 0;

		String sql = "UPDATE groupquestions gp SET gp.visibility = false WHERE id = "+
				gquestion.getId();

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<GroupQuestion> getbyFilter(String condition) {

		List<GroupQuestion> questions = new ArrayList <GroupQuestion> ();

		String sql = "SELECT * FROM groupquestions as gp WHERE  gp.visibility = true AND "
				+ condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			GroupQuestion gquestion = new GroupQuestion();

			gquestion.setId((Long) result.get("id") );
			gquestion.setName  ((String) result.get("name"));
			gquestion.setIdsurvey((Long) result.get("idsurvey") );
			gquestion.setVisibility(true);

			questions.add(gquestion);
		}
		return questions;
	}

	@Override
	public List<GroupQuestion> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
