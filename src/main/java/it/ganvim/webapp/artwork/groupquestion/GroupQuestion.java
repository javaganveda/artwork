package it.ganvim.webapp.artwork.groupquestion;

import java.util.ArrayList;
import java.util.List;

import it.ganvim.webapp.artwork.core.BaseDomain;
import it.ganvim.webapp.artwork.question.Question;

public class GroupQuestion extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String name;
	
	Long idsurvey;

	private List<Question> questions;

	public GroupQuestion () {

		this.setName("");
		this.idsurvey = 0L;
		this.questions = new ArrayList<Question>();			
	}	

	public Long getIdsurvey() {
		return idsurvey;
	}

	public void setIdsurvey(Long idsurvey) {
		this.idsurvey = idsurvey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}	
}
