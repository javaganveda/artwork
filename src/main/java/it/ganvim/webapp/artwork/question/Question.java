package it.ganvim.webapp.artwork.question;

import java.util.ArrayList;
import java.util.List;

import it.ganvim.webapp.artwork.answer.Answer;
import it.ganvim.webapp.artwork.core.BaseDomain;

public class Question extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private Long IdDomainQuestion;
	private Long IdGroupQuestion;
	private String name;
	
	//for stats
	private double averagePercentage;
	private double average;
	private String trend;
	private String dataChart;

	private List<Answer> answers;

	public Question () {

		this.setIdDomainQuestion(0L);
		this.setIdGroupQuestion(0L);
		this.answers = new ArrayList<Answer>();
		this.setName("");
		
		this.setAverage(0.0);
		this.setAveragePercentage(0.0);
		this.setTrend("");
		this.setDataChart("");
	}

	public Long getIdDomainQuestion() {
		return IdDomainQuestion;
	}

	public void setIdDomainQuestion(Long idDomainQuestion) {
		IdDomainQuestion = idDomainQuestion;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public Long getIdGroupQuestion() {
		return IdGroupQuestion;
	}

	public void setIdGroupQuestion(Long idGroupQuestion) {
		IdGroupQuestion = idGroupQuestion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}

	public String getDataChart() {
		return dataChart;
	}

	public void setDataChart(String dataChart) {
		this.dataChart = dataChart;
	}

	public double getAveragePercentage() {
		return averagePercentage;
	}

	public void setAveragePercentage(double averagePercentage) {
		this.averagePercentage = averagePercentage;
	}	
}
