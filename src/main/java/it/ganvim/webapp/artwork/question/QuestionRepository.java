package it.ganvim.webapp.artwork.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class QuestionRepository implements Repository<Long,Question> {

	DBRepository dbr = new DBRepository();

	public int add(Question entity) {

		Question question = entity;

		int res = 0;

		String sql = String.format("INSERT INTO questions (iddomainquestion, idgroupquestion,"
				+ " visibility) VALUES (%d, %d, %b)", 
				question.getIdDomainQuestion(), question.getIdGroupQuestion(),true);

		res = dbr.insert(sql);

		return res; 				
	}

	public List<Question> getAll() {

		List<Question> questions = new ArrayList<Question>();

		String sql = String.format("SELECT * FROM questions as q, domainquestions as d WHERE q.iddomainquestion = d.id AND q.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Question question = new Question();

			question.setId((Long) result.get("id"));
			question.setIdDomainQuestion((Long) result.get("iddomainquestion"));
			question.setIdGroupQuestion((Long) result.get("idgroupquestion"));
			question.setName((String) result.get("name"));
			question.setVisibility(true);

			questions.add(question);
		}
		return questions;	
	}

	@Override
	public int update(Question entity) {

		Question question = entity;

		int res = 0;

		String sql = String.format("UPDATE questions q  SET iddomainquestion = '%d', idgroupquestion"
				+ " = %d, visibility = %b WHERE q.id = %d",
				question.getIdDomainQuestion(), question.getIdGroupQuestion(), question.isVisible(),
				question.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public Question getbyId(Long id) {

		Question question = new Question();

		String sql = String.format("SELECT * FROM questions as q, domainquestions as d WHERE q.iddomainquestion = d.id AND q.visibility = true "
				+ "AND q.id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			question.setId((Long) result.get("id"));
			question.setIdDomainQuestion((Long) result.get("iddomainquestion"));
			question.setIdGroupQuestion((Long) result.get("idgroupquestion"));
			question.setName((String) result.get("name"));
			question.setVisibility(true);
		}
		return question;	
	}

	@Override
	public List<Question> getbyColumn(String column, String value) {

		List<Question> questions = new ArrayList <Question> ();

		String sql = "SELECT * FROM questions as q, domainquestions as d WHERE q.iddomainquestion = d.id AND q.visibility = true AND q." 
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Question question = new Question();

			question.setId((Long) result.get("id"));
			question.setIdDomainQuestion((Long) result.get("iddomainquestion"));
			question.setIdGroupQuestion((Long) result.get("idgroupquestion"));
			question.setName((String) result.get("name"));
			question.setVisibility(true);

			questions.add(question);
		}
		return questions;
	}

	@Override
	public int delete(Question entity) {

		Question question = entity;

		int res = 0;

		String sql = String.format("UPDATE questions q SET q.visibility = false WHERE id = %d",
				question.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<Question> getbyFilter(String condition) {

		List<Question> questions = new ArrayList <Question> ();

		String sql = "SELECT * FROM questions as q, domainquestions as d WHERE q.iddomainquestion = d.id AND  q.visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Question question = new Question();

			question.setId((Long) result.get("id"));
			question.setIdDomainQuestion((Long) result.get("iddomainquestion"));
			question.setIdGroupQuestion((Long) result.get("idgroupquestion"));
			question.setName((String) result.get("name"));
			question.setVisibility(true);

			questions.add(question);
		}
		return questions;
	}

	@Override
	public List<Question> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}