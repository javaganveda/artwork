package it.ganvim.webapp.artwork.core;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBRepository {

	private static String URL;
	private static String USER = "";
	private static String PASSWORD = "";

	public  DBRepository() {

		String dbAddress  = "";
		String user  = "";
		String password  = "";
		
		InputStream config = getClass().getClassLoader().getResourceAsStream("/dbaddress.sys");

		try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(config))) {

			dbAddress 	= bufferedReader.readLine();
			user  		= bufferedReader.readLine();
			password 	= bufferedReader.readLine();
			
			URL 		= dbAddress;
			USER 		= user;
			PASSWORD	= password;

		}catch (Exception e) {
			System.out.println(" file Open Error ");
			e.printStackTrace();
			//URL = "jdbc:mysql://192.168.30.133:3306/artwork?serverTimezone=UTC&autoReconnect=true&useSSl=false";
			//USER = "root";
			//PASSWORD = "test";
		}
	}

	public int insert(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

			statement = connection.createStatement();

			statement.executeUpdate(sql);
		}
		catch (SQLException e) {
			System.out.println(" DataBase Insert Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
/*
	public ResultSet select(String sql) {

		ResultSet resultSet = null;

		Connection connection = null;
		Statement statement = null;

		try {

			connection = DriverManager.getConnection(URL, USER, PASSWORD);

			statement = connection.createStatement();

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}

		try{

			resultSet = statement.executeQuery(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Select Error ");
			e.printStackTrace();
		}

		return resultSet;
	}
*/
	
	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			ResultSetMetaData rsmd = resultSet.getMetaData();
			
			int columnCount = rsmd.getColumnCount();

			while(resultSet.next()) {
				
				Map<String, Object> result = new HashMap<>();

				for (int i = 1; i <= columnCount; i++) {
					
					String columnName = rsmd.getColumnName(i);
					result.put(columnName, resultSet.getObject(columnName));
				}
				
				results.add(result);
			}
		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
		return results;
	}
	
	public int update(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

			statement = connection.createStatement();
			
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Update Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}

	public int delete(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){

			statement = connection.createStatement();

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Delete Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}	
}