package it.ganvim.webapp.artwork.core;

import java.io.Serializable;
import java.util.List;

import it.ganvim.webapp.artwork.survey.Survey;

public interface Repository<ID extends Serializable, D extends BaseDomain<ID>> {
	public int add(D entity);
	
	public int update(D entity);
	
	public List<D> getAll();
	
	public D getbyId(ID id);
	
	public List<D> getbyColumn(String column, String value);
	
	public List<D> getbyFilter(String condition);
	
	public int delete(D entity); 
	
	public List<D> getAllStats();
	
	public Survey getForStats(ID id);
	
	public boolean isModified(ID id);
	
}
