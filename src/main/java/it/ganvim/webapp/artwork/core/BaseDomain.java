package it.ganvim.webapp.artwork.core;

import java.io.Serializable;

public abstract class BaseDomain<ID extends Serializable > implements Serializable {

	private static final long serialVersionUID = 1L;

	private ID id;

	private Boolean visibility;

	public BaseDomain() {

		this.visibility = true;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
}
