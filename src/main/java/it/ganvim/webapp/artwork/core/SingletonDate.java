package it.ganvim.webapp.artwork.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.choice.Choice;

@Dependent
@Singleton
public class SingletonDate extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;
	
	//Static
	
	public final static String MESSOK = "success";
	public final static String MESSWORN = "warning";
	public final static String MESSERROR = "danger";
	public final static String MESSINFO = "info";

	//Common
	
	public String message;
	public String messSeverity;
	
	//Quest
	
	public List<Choice> allChoices;
	public Map<Long, String> choices;

	public Long customer;
	public Long idSurvey; 
	public String language;
	public String title;
	public String flag;
	public String question;
	public String quitKey;
	public String nextKey;
	public int nQuest;
	
	//Admin
	
	public String username;
	public Long idLogin;
	public String level;
	public Long idUser;
	
	private String chartType;
	
	public boolean[] panelCollapsed;

	public SingletonDate () {
		
		setAllChoices(new ArrayList<Choice>());
		setChoices(new HashMap<Long, String>());
		customer = 0L;
		idSurvey = 0L;
		language = "";
		title = "";
		flag = "";
		question = "";
		quitKey = "";
		nextKey = "";
		nQuest = 0;
		
		username = "";
		level = "";
		idLogin = 0L;
		idUser = 0L;	
		
		chartType = "chart";
		
		panelCollapsed = new boolean[100];
		for(int i=0; i<100; i++) {
			panelCollapsed[i] = true;		
		}	

	}

	public List<Choice> getAllChoices() {
		return allChoices;
	}

	public void setAllChoices(List<Choice> allChoices) {
		this.allChoices = allChoices;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}
	
	public int getnQuest() {
		return nQuest;
	}

	public void setnQuest(int nQuest) {
		this.nQuest = nQuest;
	}

	public Map<Long, String> getChoices() {
		return choices;
	}

	public void setChoices(Map<Long, String> choices) {
		this.choices = choices;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessSeverity() {
		return messSeverity;
	}

	public void setMessSeverity(String messSeverity) {
		this.messSeverity = messSeverity;
	}

	public Long getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(Long idSurvey) {
		this.idSurvey = idSurvey;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Long getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(Long idLogin) {
		this.idLogin = idLogin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getQuitKey() {
		return quitKey;
	}

	public void setQuitKey(String quitKey) {
		this.quitKey = quitKey;
	}

	public String getNextKey() {
		return nextKey;
	}

	public void setNextKey(String nextKey) {
		this.nextKey = nextKey;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public boolean[] getPanelCollapsed() {
		return panelCollapsed;
	}

	public void setPanelCollapsed(boolean[] panelCollapsed) {
		this.panelCollapsed = panelCollapsed;
	}

	public String getChartType() {
		return chartType;
	}

	public void setChartType(String chartType) {
		this.chartType = chartType;
	}
}
