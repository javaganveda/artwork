package it.ganvim.webapp.artwork.core;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

public abstract class Controller<ID extends Serializable, D extends BaseDomain<ID>> implements Serializable {

	private static final long serialVersionUID = 1L;

	Map<String, String> parameters;
	
	@Inject
	private SingletonDate sd = new SingletonDate();
	
	@PostConstruct
	public void postConstruct() {
		parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		init();
	}

	public abstract void init(); 
	

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
	
	public SingletonDate getSd() {
		return sd;
	}

	public void setSd(SingletonDate sd) {
		this.sd = sd;
	}

	public void redirect(String page) {
		
		try {

			FacesContext.getCurrentInstance().getExternalContext().redirect(page);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}		
	}
	
	public void logout() {
		
		sd.setIdLogin(0L);
		sd.setLevel("");
		sd.setUsername("");
		this.redirect("../login.xhtml");		
	}
	
	public void changePage(String page) {
		
		sd.setMessage("");
		sd.setIdUser(0L);
		this.redirect(page);		
	}
	
	public void myProfile() {
		
		sd.setIdUser(sd.getIdLogin());
		sd.setMessage("");
		this.redirect("salvautenti.xhtml");
	}
}