package it.ganvim.webapp.artwork.suggest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class SuggestRepository implements Repository<Long,Suggest> {

	DBRepository dbr = new DBRepository();

	public int add(Suggest entity) {

		Suggest suggest = entity;

		int res = 0;

		List<Suggest> suggests = new ArrayList<Suggest>();

		suggest.setSuggest(suggest.getSuggest().replace ("'", "''"));

		String condition = " suggest = '" + suggest.getSuggest() + "' AND suggest = '" + 
				suggest.getSuggest() + "' " ;

		suggests = this.getbyFilter(condition);

		if (suggests.size() > 0) {
			return 1;
		}
		else {

			suggest.setSuggest(suggest.getSuggest().replace ("'", "''"));

			String sql = String.format("INSERT INTO suggests (suggest,"
					+ " visibility) VALUES "
					+ "('%s', %b)", 
					suggest.getSuggest(), suggest.isVisible());

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<Suggest> getAll() {

		List<Suggest> suggests = new ArrayList <Suggest> ();

		String sql = String.format("SELECT * FROM suggests as s WHERE s.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Suggest suggest = new Suggest();

			suggest.setId((Long) result.get("id") );
			suggest.setSuggest((String) result.get("suggest"));
			suggest.setVisibility(true);

			suggests.add(suggest);
		}
		return suggests;	
	}

	@Override
	public int update(Suggest entity) {

		Suggest suggest = entity;

		int res = 0;

		suggest.setSuggest(suggest.getSuggest().replace ("'", "''"));

		String sql = "UPDATE suggests s  SET suggest = '" + suggest.getSuggest()
				+ "', visibility = " + suggest.isVisible() + " WHERE id = "
				+ suggest.getId();
				
		res = dbr.update(sql);

		return res;
	}

	@Override
	public Suggest getbyId(Long id) {

		Suggest suggest = new Suggest ();

		String sql = String.format("SELECT * FROM suggests as s WHERE s.visibility = true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			suggest.setId((Long) result.get("id") );
			suggest.setSuggest((String) result.get("suggest"));
			suggest.setVisibility(true);
		}
		return suggest;	
	}

	@Override
	public List<Suggest> getbyColumn(String column, String value) {

		List<Suggest> suggests = new ArrayList <Suggest> ();

		String sql = "SELECT * FROM suggests as s WHERE  s.visibility = true AND s."
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Suggest suggest = new Suggest ();

			suggest.setId((Long) result.get("id") );
			suggest.setSuggest((String) result.get("suggest"));
			suggest.setVisibility(true);

			suggests.add(suggest);
		}
		return suggests;
	}

	@Override
	public int delete(Suggest entity) {

		Suggest suggest = entity;

		int res = 0;

		String sql = String.format("UPDATE suggests s SET s.visibility = false WHERE id = %d",
				suggest.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<Suggest> getbyFilter(String condition) {

		List<Suggest> suggests = new ArrayList <Suggest> ();

		String sql = "SELECT * FROM suggests as s WHERE  s.visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Suggest suggest = new Suggest();

			suggest.setId((Long) result.get("id") );
			suggest.setSuggest((String) result.get("suggest"));
			suggest.setVisibility(true);

			suggests.add(suggest);
		}
		return suggests;
	}

	@Override
	public List<Suggest> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}