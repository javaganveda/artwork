package it.ganvim.webapp.artwork.suggest;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class Suggest extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String suggest;

	public Suggest () {

		this.setSuggest ("");
	}

	public String getSuggest() {
		return suggest;
	}

	public void setSuggest(String suggest) {
		this.suggest = suggest;
	}	
}