package it.ganvim.webapp.artwork.suggest;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class SuggestController extends Controller<Long, Suggest> {

	private static final long serialVersionUID = 1L;

	private List<Suggest> suggests;

	@Inject
	private Repository<Long, Suggest> suggestRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.setSuggests(new ArrayList<Suggest>());
		this.list();
	}

	public void list() {
		this.suggests = suggestRepository.getAll();
	}

	public List<Suggest> getSuggests() {
		return suggests;
	}

	public void setSuggests(List<Suggest> suggests) {
		this.suggests = suggests;
	}	
}