package it.ganvim.webapp.artwork.test;

import java.util.ArrayList;
import java.util.List;

import it.ganvim.webapp.artwork.answer.Answer;
import it.ganvim.webapp.artwork.answer.AnswerRepository;
import it.ganvim.webapp.artwork.choice.Choice;
import it.ganvim.webapp.artwork.choice.ChoiceRepository;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswer;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswerRepository;
import it.ganvim.webapp.artwork.domainquestion.DomainQuestion;
import it.ganvim.webapp.artwork.domainquestion.DomainQuestionRepository;
import it.ganvim.webapp.artwork.email.Email;
import it.ganvim.webapp.artwork.email.EmailRepository;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestion;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestionRepository;
import it.ganvim.webapp.artwork.joinsurvey.JoinSurvey;
import it.ganvim.webapp.artwork.joinsurvey.JoinSurveyRepository;
import it.ganvim.webapp.artwork.login.Login;
import it.ganvim.webapp.artwork.login.LoginRepository;
import it.ganvim.webapp.artwork.question.Question;
import it.ganvim.webapp.artwork.question.QuestionRepository;
import it.ganvim.webapp.artwork.suggest.Suggest;
import it.ganvim.webapp.artwork.suggest.SuggestRepository;
import it.ganvim.webapp.artwork.survey.Survey;
import it.ganvim.webapp.artwork.survey.SurveyRepository;
import it.ganvim.webapp.artwork.user.User;
import it.ganvim.webapp.artwork.user.UserRepository;

public class MainTest {

	public static void main(String[] args) {
		
		//Test ANSWER REPOSITORY
		List<Answer> answers = new ArrayList<Answer>();
		AnswerRepository ar = new AnswerRepository();
		Answer answer = new Answer();
		
		answer.setId(1L);
		answer.setIdDomainAnswer(5L);
		answer.setIdQuestion(6L);
		answer.setVisibility(true);
		
		//ar.add(answer);
		//ar.update(answer);
		//System.out.println(ar.delete(answer));
		//answer = ar.getbyId(2L);
		
		//System.out.println(" " +answer.getId().longValue() + " " + answer.getIdQuestion());
		
		//answers = ar.getAll();
		//answers = ar.getbyFilter("idquestion = 6");
		//answers = ar.getbyColumn("idquestion", "6");
		System.out.println("main "+answers.size());
		for(int i=0; i<answers.size(); i++) {
			
			System.out.println(answers.get(i).getId().longValue() + " " +answers.get(i).getName());	
		}
		
		//Test CHOICE REPOSITORY
		List<Choice> choices = new ArrayList<Choice>();
		ChoiceRepository cr = new ChoiceRepository();
		Choice choice = new Choice();
		
		choice.setId(1L);
		choice.setCustomer(5L);
		choice.setIdanswer(5L);
		choice.setSelected(false);
		choice.setVisibility(true);
		
		//cr.add(choice);
		//cr.update(choice);
		//System.out.println(cr.delete(choice));
		
		//choice = cr.getbyId(2L);		
		//System.out.println(" " +choice.getId().longValue() + " " + choice.getCustomer());
		
		//choices = cr.getAll();
		//choices = cr.getbyFilter("customer = 5");
		//choices = cr.getbyColumn("customer", "23");
		System.out.println("main "+choices.size());
		for(int i=0; i<choices.size(); i++) {
			
			System.out.println(choices.get(i).getId().longValue() + " " +choices.get(i).getCustomer());	
		}
		
		
		//Test DOMAINANSWER REPOSITORY
		List<DomainAnswer> domainAnswers = new ArrayList<DomainAnswer>();
		DomainAnswerRepository dar = new DomainAnswerRepository();
		DomainAnswer domainAnswer = new DomainAnswer();
		
		domainAnswer.setId(28L);
		domainAnswer.setName("prova3");
		domainAnswer.setValue(10.60);;
		domainAnswer.setVisibility(true);
		
		//dar.add(domainAnswer);
		//dar.update(domainAnswer);
		//System.out.println(dar.delete(domainAnswer));
		
		//domainAnswer = dar.getbyId(28L);		
		//System.out.println(" " +domainAnswer.getId().longValue() + " " + domainAnswer.getName());
		
		//domainAnswers = dar.getAll();
		//domainAnswers = dar.getbyFilter("name like '%po%'");
		//domainAnswers = dar.getbyColumn("name", "3");
		System.out.println("main "+domainAnswers.size());
		for(int i=0; i<domainAnswers.size(); i++) {
			
			System.out.println(domainAnswers.get(i).getId().longValue() + " " +domainAnswers.get(i).getName());	
		}
		

		//Test DOMAINQUESTION REPOSITORY
		List<DomainQuestion> domainQuestions = new ArrayList<DomainQuestion>();
		DomainQuestionRepository dqr = new DomainQuestionRepository();
		DomainQuestion domainQuestion = new DomainQuestion();
		
		domainQuestion.setId(18L);
		domainQuestion.setName("prova2");
		domainQuestion.setVisibility(true);
		
		//dqr.add(domainQuestion);
		//dqr.update(domainQuestion);
		//System.out.println(dqr.delete(domainQuestion));
		
		//domainQuestion = dqr.getbyId(18L);		
		//System.out.println(" " +domainQuestion.getId().longValue() + " " + domainQuestion.getName());
		
		//domainQuestions = dqr.getAll();
		//domainQuestions = dqr.getbyFilter("name like '%co%'");
		//domainQuestions = dqr.getbyColumn("name", "Domus");
		System.out.println("main "+domainQuestions.size());
		for(int i=0; i<domainQuestions.size(); i++) {
			
			System.out.println(domainQuestions.get(i).getId().longValue() + " " +domainQuestions.get(i).getName());	
		}
		
		
		//Test EMAIL REPOSITORY
		List<Email> emails = new ArrayList<Email>();
		EmailRepository er = new EmailRepository();
		Email email = new Email();
		
		email.setId(2L);
		email.setEmail("email@libero.com");;
		email.setVisibility(true);
		
		//er.add(email);
		//er.update(email);
		//System.out.println(er.delete(email));
		//email = er.getbyId(2L);
		//System.out.println(" " +email.getId().longValue() + " " + email.getEmail());
		
		//emails = er.getAll();
		//emails = er.getbyFilter("email like '%libero%'");
		//emails = er.getbyColumn("email", "prova@libero.it");
		System.out.println("main "+emails.size());
		for(int i=0; i<emails.size(); i++) {
			
			System.out.println(emails.get(i).getId().longValue() + " " +emails.get(i).getEmail());	
		}
		
		//Test  GROUPQUESTIONREPOSITORY
		List<GroupQuestion> groupquestions = new ArrayList<GroupQuestion>();
		GroupQuestionRepository gqr = new GroupQuestionRepository();
		GroupQuestion groupquestion = new GroupQuestion();
		
		groupquestion.setId(18L);
		groupquestion.setName("20) Prova domanda...");
		groupquestion.setIdsurvey(1L);
		groupquestion.setVisibility(true);
		
		//gqr.add(groupquestion);
		//gqr.update(groupquestion);
		//System.out.println(gqr.delete(groupquestion));
		//groupquestion = gqr.getbyId(18L);
		//System.out.println(" " +groupquestion.getId().longValue() + " " + groupquestion.getName());
		
		//groupquestions = gqr.getAll();
		//groupquestions = gqr.getbyFilter("name like '%come%'");
		//groupquestions = gqr.getbyColumn("name", "5)");
		System.out.println("main "+groupquestions.size());
		for(int i=0; i<groupquestions.size(); i++) {
			
			System.out.println(groupquestions.get(i).getId().longValue() + " " +groupquestions.get(i).getName());	
		}

		//Test  QUESTIONREPOSITORY
		List<Question> questions = new ArrayList<Question>();
		QuestionRepository qr = new QuestionRepository();
		Question question = new Question();
		
		question.setId(16L);
		question.setIdGroupQuestion(4L);;
		question.setIdDomainQuestion(5L);
		question.setVisibility(true);
		
		//qr.add(question);
		//qr.update(question);
		//System.out.println(qr.delete(question));
		//question = qr.getbyId(16L);
		//System.out.println(" " +question.getId().longValue() + " " + groupquestion.getName());
		
		//questions = qr.getAll();
		//questions = qr.getbyFilter("idgroupquestion = 4");
		//questions = qr.getbyColumn("idgroupquestion", "4");
		System.out.println("main "+questions.size());
		for(int i=0; i<questions.size(); i++) {
			
			System.out.println(questions.get(i).getId().longValue() + " " +questions.get(i).getName());	
		}
		

		//Test  SUGGESTREPOSITORY
		List<Suggest> suggests = new ArrayList<Suggest>();
		SuggestRepository sr = new SuggestRepository();
		Suggest suggest = new Suggest();
		
		suggest.setId(1L);
		suggest.setSuggest("prova...2");;
		suggest.setVisibility(true);
		
		//sr.add(suggest);
		//sr.update(suggest);
		//System.out.println(sr.delete(suggest));
		//suggest = sr.getbyId(1L);
		//System.out.println(" " +suggest.getId().longValue() + " " + suggest.getSuggest());
		
		//suggests = sr.getAll();
		//suggests = sr.getbyFilter("suggest like '%34%'");
		//suggests = sr.getbyColumn("suggest", "3453");
		System.out.println("main "+suggests.size());
		for(int i=0; i<suggests.size(); i++) {
			
			System.out.println(suggests.get(i).getId().longValue() + " " +suggests.get(i).getSuggest());	
		}
		

		//Test  SURVEYREPOSITORY
		List<Survey> surveys = new ArrayList<Survey>();
		SurveyRepository sur = new SurveyRepository();
		Survey survey = new Survey();
		
		survey.setId(2L);
		survey.setName("prova...5555");
		survey.setTypology("qwe");
		survey.setDescription("tre");
		survey.setVisibility(true);
		
		//sur.add(survey);
		//sur.update(survey);
		//System.out.println(sur.delete(survey));
		
		//survey = sur.getbyId(1L);
		//System.out.println(" " + survey.getId().longValue() + " " + survey.getName() + " " +
		//survey.getGroupQuestions().get(0).getName() + " " + 
		//survey.getGroupQuestions().get(0).getQuestions().get(0).getIdDomainQuestion() + " " + 
		//survey.getGroupQuestions().get(0).getQuestions().get(0).getAnswers().get(0).getIdDomainAnswer());
	
	
		//survey = sur.getForStats(1L);
		//System.out.println(" " + survey.getId().longValue() + " " + survey.getName() + " " +
		//survey.getGroupQuestions().get(0).getName() + " " + 
		//survey.getGroupQuestions().get(0).getQuestions().get(0).getIdDomainQuestion() + " " + 
		//survey.getGroupQuestions().get(0).getQuestions().get(0).getAnswers().get(0).getIdDomainAnswer() + " " +
		//survey.getGroupQuestions().get(0).getQuestions().get(0).getAnswers().get(0).getChoice().get(0).isSelected());
		
		//surveys = sur.getAll();
		//surveys = sur.getbyFilter("name like '%55%'");
		//surveys = sur.getbyColumn("typology", "Visitatoreadulto");
		System.out.println("main "+surveys.size());
		for(int i=0; i<surveys.size(); i++) {
			
			System.out.println(surveys.get(i).getId().longValue() + " " +surveys.get(i).getName());	
		}
		

		//Test  JOINSURVEYREPOSITORY
		List<JoinSurvey> joinSurveys = new ArrayList<JoinSurvey>();
		JoinSurveyRepository joinsur = new JoinSurveyRepository();
		JoinSurvey joinSurvey = new JoinSurvey();
		
		joinSurvey.setId(2L);
		joinSurvey.setJKey("prova000");
		joinSurvey.setIdSurvey(5L);
		joinSurvey.setVisibility(true);
		
		//joinsur.add(joinSurvey);
		//joinsur.update(joinSurvey);
		//System.out.println(joinsur.delete(joinSurvey));
		
		//joinSurvey = joinsur.getbyId(1L);
		//System.out.println(" " + joinSurvey.getId().longValue() + " " + joinSurvey.getKey());

		
		//joinSurveys = joinsur.getAll();
		//joinSurveys = joinsur.getbyFilter("jkey like '%00%'");
		//joinSurveys = joinsur.getbyColumn("jkey", "prova");
		System.out.println("main "+surveys.size());
		for(int i=0; i<joinSurveys.size(); i++) {
			
			System.out.println(joinSurveys.get(i).getId().longValue() + " " +joinSurveys.get(i).getJKey());	
		}

		
		
		//------------------------------------------
		//Test USER REPOSITORY
		LoginRepository lr = new LoginRepository();
		
		List<Login> logins = new ArrayList<Login>();
		
		Login  lg = new Login();
		
			
		//Test CHECKLOGIN
		lg.setUsername("admin");
		lg.setPassword("admin");
	
		//lg = lr.checkLogin(lg);
		//System.out.println(lg.getLevel());
		

		
		
		//Test USER REPOSITORY
		User user = new User();
		List<User> users = new ArrayList<User>();
		UserRepository ur = new UserRepository();
		
		user.setId(8L);
		user.setSurname("d'Alise");
		user.setName("Giovanni");
		user.setAddress("Via Vittorio Emanuele III, n� 341");
		user.setCap("80015");
		user.setCity("Casalnuovo di Napoli");
		user.setRole("Dirigente");	
		user.setVisibility(true);
		
		//ur.add(user);
		//ur.update(user);
		//System.out.println(ur.delete(user));
		//user = ur.getbyId(user.getId());
		
		//System.out.println(" " + user.getId().longValue() + " " + user.getRole());
		
		//users = ur.getAll();
		//users = ur.getbyFilter("name like '%Gio%'");
		//users = ur.getbyColumn(user.ROLE, user.getRole());
		System.out.println("main "+users.size());
		for(int i=0; i<users.size(); i++) {
			
			System.out.println(users.get(i).getId().longValue() + " " + user.getRole());	
		}
		
		
		//Test LOGIN REPOSITORY
		lg.setUser(user);
		lg.setId(1L);
		lg.setUsername("admin");
		lg.setPassword("admin");
		lg.setLevel("admin");
		lg.setEnabled(true);
		lg.setVisibility(true);
		
		//lr.add(lg);
		//lr.update(lg);
		//lr.delete(lg);
		//lg = lr.getbyId(lg.getId());
		
		//System.out.println(" " + lg.getId().longValue() + " " +lg.getLevel());
		
		//logins = lr.getAll();
		//logins = lr.getbyFilter("username like '%ad%'");
		//logins = lr.getbyColumn("level", lg.getLevel());
		System.out.println("main "+logins.size());
		for(int i=0; i<logins.size(); i++) {
			
			System.out.println(logins.get(i).getId().longValue());			
		}		
	}
}
