package it.ganvim.webapp.artwork.stats;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class StatsListController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;

	//private List<Stats> stats;
	private List<Survey> surveys;

	@Inject
	private Repository<Long, Survey>  statsRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
	    this.setSurveys(new ArrayList<Survey>());
		
		this.list();
	}
	
	public void list() {
		this.setSurveys(statsRepository.getAllStats());
	}
	
	public void openStats(Long idSurvey) {	
		this.getSd().setIdSurvey(idSurvey);
		this.getSd().setMessage("");
		this.redirect("visualizzastatistiche.xhtml");	
	}

	public List<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(List<Survey> surveys) {
		this.surveys = surveys;
	}
}