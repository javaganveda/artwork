package it.ganvim.webapp.artwork.stats;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.answer.Answer;
import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswer;
import it.ganvim.webapp.artwork.domainquestion.DomainQuestion;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestion;
import it.ganvim.webapp.artwork.question.Question;
import it.ganvim.webapp.artwork.survey.Survey;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class StatsViewController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;

	private Survey survey;

	@Inject
	private Repository<Long, Survey> statsRepository;
	@Inject
	private Repository<Long, GroupQuestion> gqRepository;
	@Inject
	private Repository<Long, Question> qRepository;
	@Inject
	private Repository<Long, Answer> aRepository;
	@Inject
	private Repository<Long, DomainQuestion> dqRepository;
	@Inject
	private Repository<Long, DomainAnswer> daRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
		//id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 1L;
		
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.survey = new Survey();

		this.list();
	}

	public void list() {
		this.setSurvey(statsRepository.getForStats(this.getSd().getIdSurvey()));
	}
	public void refreshCharter() {
		this.redirect("visualizzastatistiche.xhtml");
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public Repository<Long, Question> getqRepository() {
		return qRepository;
	}

	public void setqRepository(Repository<Long, Question> qRepository) {
		this.qRepository = qRepository;
	}

	public Repository<Long, Answer> getaRepository() {
		return aRepository;
	}

	public void setaRepository(Repository<Long, Answer> aRepository) {
		this.aRepository = aRepository;
	}

	public Repository<Long, DomainQuestion> getDqRepository() {
		return dqRepository;
	}

	public void setDqRepository(Repository<Long, DomainQuestion> dqRepository) {
		this.dqRepository = dqRepository;
	}

	public Repository<Long, DomainAnswer> getDaRepository() {
		return daRepository;
	}

	public void setDaRepository(Repository<Long, DomainAnswer> daRepository) {
		this.daRepository = daRepository;
	}

	public Repository<Long, GroupQuestion> getGqRepository() {
		return gqRepository;
	}

	public void setGqRepository(Repository<Long, GroupQuestion> gqRepository) {
		this.gqRepository = gqRepository;
	}
}