package it.ganvim.webapp.artwork.user;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.login.Login;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class UserListController extends Controller<Long, User> {

	private static final long serialVersionUID = 1L;

	private List<Login> logins;

	@Inject
	private Repository<Long, Login> loginRepository;
	
	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
		
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		logins = new ArrayList<Login>();
		this.list();
	}

	public void list() {
		logins = loginRepository.getAll();
	}

	public void delete(Long id, Long iduser) {

		int res = 0;
		
		User user = new User();
		Login login = new Login();

		user.setId(iduser);
		login.setId(id);

		login.setUser(user);

		loginRepository.delete(login);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");	
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}

		// Redirigere la pagina
		this.redirect("utenti.xhtml");

	}
	
	public void update(Long iduser) {

		this.getSd().setIdUser(iduser);
		this.getSd().setMessage("");	
		this.redirect("salvautenti.xhtml");		
	}

	public List<Login> getLogins() {
		return logins;
	}

	public void setLogins(List<Login> logins) {
		this.logins = logins;
	}
}