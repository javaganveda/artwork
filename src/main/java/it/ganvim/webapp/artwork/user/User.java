package it.ganvim.webapp.artwork.user;

//import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class User extends BaseDomain<Long> {

	// COSTANTI PER RICERCA
	public final String SURNAME = "surname";
	public final String NAME = "name";
	public final String CITY = "city";
	public final String ROLE = "role";
	
	private static final long serialVersionUID = 1L;

	@Size(min = 1, message = "Campo obbligatorio")
	private String surname;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String name;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String address;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String cap;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String city;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String role;

	public User () {

		this.setSurname("");
		this.setName("");
		this.setAddress("");
		this.setCap("");
		this.setCity("");
		this.setRole("");	
	}	

	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}