package it.ganvim.webapp.artwork.user;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.login.Login;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class UserAddUpdateController extends Controller<Long, User> {

	private static final long serialVersionUID = 1L;

	private Login login;

	@Inject
	private Repository<Long, Login> loginRepository;
	
	public void init() {

		//id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;

		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		if (this.getSd().getIdUser().equals(0L)) {
			login = new Login();
			login.setId(this.getSd().getIdUser());
			login.getUser().setId(this.getSd().getIdUser());
			login.setLevel("Admin");
		}
		else
		{
			login = loginRepository.getbyId(this.getSd().getIdUser());
		}
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public void addUpdateUser() {

		int res = 0;

		if(login.getId().equals(0L)) {

			res = loginRepository.add(login);
			if(res == 0) {	
				this.getSd().setMessage("Inserimento avvenuto correttamente");	
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == 1) {			
				this.getSd().setMessage("Utente gi� presente in archivio");	
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}

			if(res == -1) {		
				this.getSd().setMessage("Errore nell'inserimento");	
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}	
		}
		else{

			res = loginRepository.update(login);

			if(res == 0) {			
				this.getSd().setMessage("Modifica avvenuta correttamente");	
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == 1) {				
				this.getSd().setMessage("Utente gi� presente in archivio");	
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}

			if(res == -1) {				
				this.getSd().setMessage("Errore nella modifica");	
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}
		}
	}
}
