package it.ganvim.webapp.artwork.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class UserRepository implements Repository<Long,User> {

	DBRepository dbr = new DBRepository();

	public int add(User entity) {

		User user = entity;

		int res = 0;

		List<User> users = new ArrayList<User>();

		user.setSurname(user.getSurname().replace ("'", "''"));
		user.setName  (user.getName().replace ("'", "''"));

		String condition = " surname = '" + user.getSurname() + "' AND name = '" + 
				user.getName() + "' " ;

		users = this.getbyFilter(condition);

		if (users.size() > 0) {
			return 1;
		}
		else {

			user.setAddress(user.getAddress().replace ("'", "''"));
			user.setCap(user.getCap().replace ("'", "''"));
			user.setCity(user.getCity().replace ("'", "''"));
			user.setRole(user.getRole().replace ("'", "''"));			

			String sql = String.format("INSERT INTO users (name, surname, address, cap, city,"
					+ " role, visibility) VALUES "
					+ "('%s','%s','%s', '%s','%s','%s', %b)", 
					user.getName(), user.getSurname(), user.getAddress(), user.getCap(),
					user.getCity(), user.getRole(), true);

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<User> getAll() {

		List<User> users = new ArrayList <User> ();

		String sql = String.format("SELECT * FROM users as u WHERE u.visibility=true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId((Long) result.get("id"));
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			users.add(user);
		}
		return users;	
	}

	@Override
	public int update(User entity) {

		User user = entity;

		int res = 0;

		user.setSurname(user.getSurname().replace ("'", "''"));
		user.setName  (user.getName().replace ("'", "''"));
		user.setAddress(user.getAddress().replace ("'", "''"));
		user.setCap(user.getCap().replace ("'", "''"));
		user.setCity(user.getCity().replace ("'", "''"));
		user.setRole(user.getRole().replace ("'", "''"));	

		String sql = String.format("UPDATE users u  SET name = '%s', surname = '%s', address = '%s',"
				+ " cap = '%s', city = '%s', role = '%s', visibility = %b WHERE u.id = %d",
				user.getName(), user.getSurname(), user.getAddress(), user.getCap(),
				user.getCity(), user.getRole(), user.isVisible(), user.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public User getbyId(Long id) {

		User user = new User();

		String sql = String.format("SELECT * FROM users as u WHERE u.visibility=true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			user.setId((Long) result.get("id") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);
		}
		return user;	
	}

	@Override
	public List<User> getbyColumn(String column, String value) {

		List<User> users = new ArrayList <User> ();

		String sql = "SELECT * FROM users as u WHERE  u.visibility=true AND u." + column + " = '" + 
		value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId((Long) result.get("id") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			users.add(user);
		}
		return users;
	}

	@Override
	public int delete(User entity) {

		User user = entity;

		int res = 0;

		String sql = String.format("UPDATE users u SET u.visibility = false WHERE id = %d", user.getId());
		System.out.println(sql);
		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<User> getbyFilter(String condition) {

		List<User> users = new ArrayList <User> ();

		String sql = "SELECT * FROM users as u WHERE  u.visibility = true AND " + condition;
		System.out.println(sql);
		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId((Long) result.get("id") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			users.add(user);
		}
		return users;
	}

	@Override
	public List<User> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}