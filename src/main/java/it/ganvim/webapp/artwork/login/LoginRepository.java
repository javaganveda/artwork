package it.ganvim.webapp.artwork.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;
import it.ganvim.webapp.artwork.user.User;
import it.ganvim.webapp.artwork.user.UserRepository;

@Dependent
@Singleton
public class LoginRepository implements Repository<Long, Login> {

	private DBRepository dbr = new DBRepository();

	public int add(Login entity) {

		int res = 0;

		Login login = entity;

		List<Login> logins = new ArrayList<Login>();
		List<User> users = new ArrayList<User>();

		logins = this.getbyColumn("username", login.getUsername());

		if (logins.size() > 0) {
			return 2;
		}
		else {

			UserRepository ur = new UserRepository();

			login.getUser().setName((login.getUser().getName().replace ("'", "''")));
			login.getUser().setSurname((login.getUser().getSurname().replace ("'", "''")));

			String condition = "surname = '" + login.getUser().getSurname() + "' AND name = '" + 
					login.getUser().getName() + "' " ;

			users = ur.getbyFilter(condition);


			if (users.size() > 0) {
				return 1;
			}
			else {

				res = res + ur.add(login.getUser());

				users = ur.getbyFilter(condition);

				login.setUser(users.get(0));

				if (res == 0) {

					login.setUsername (login.getUsername().replace ("'", "''"));
					login.setPassword (login.getPassword().replace ("'", "''"));

					String sql = String.format("INSERT INTO Login (username, password, level, enable, iduser, visibility)"
							+ " VALUES ('%s','%s','%s',%b, %d, %b)",
							login.getUsername(), login.getPassword(), login.getLevel(), 
							login.getEnabled(), login.getUser().getId(),true);

					res = dbr.insert(sql);
				}
				else {

					res = -1;
				}
			}
		}
		return res;
	}

	public List<Login> getAll() {

		List<Login> logins = new ArrayList <Login> ();

		String sql = String.format("SELECT * FROM login as l, users as u WHERE l.visibility=true AND l.iduser = u.id");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Login login = new Login();

			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setLevel((String) result.get("level"));

			Integer i = (Integer) result.get("enable");
			if(i.equals(0)) {
				login.setEnabled(false);
			}
			else {
				login.setEnabled(true);
			}

			login.setVisibility(true);

			user.setId((Long) result.get("iduser") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			login.setUser(user);

			logins.add(login);
		}
		return logins;	
	}

	@Override
	public int update(Login entity) {

		Login login = entity;

		int res = 0;

		login.setUsername(login.getUsername().replace ("'", "''"));
		login.setPassword(login.getPassword().replace ("'", "''"));

		String sql = String.format("UPDATE login l  SET username = '%s', password = '%s', "
				+ "level = '%s', enable = %b, visibility =  %b WHERE l.id = %d",
				login.getUsername(), login.getPassword(), login.getLevel(), login.getEnabled(),
				login.isVisible(), login.getId());

		res = dbr.update(sql);

		UserRepository ur = new UserRepository();

		res = res + ur.update(login.getUser());

		return res;
	}

	@Override
	public Login getbyId(Long id) {

		Login login = new Login();
		User user = new User();

		String sql = String.format("SELECT * FROM login as l, users as u WHERE l.visibility = true AND "
				+ "l.iduser = u.id AND l.id=%d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setLevel((String) result.get("level"));

			Integer i = (Integer) result.get("enable");
			if(i.equals(0)) {
				login.setEnabled(false);
			}
			else {
				login.setEnabled(true);
			}

			login.setVisibility(true);

			user.setId((Long) result.get("iduser") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);
		}

		login.setUser(user);

		return login;	
	}

	@Override
	public List<Login> getbyColumn(String column, String value) {

		List<Login> logins = new ArrayList <Login> ();

		String sql;

		value = value.replace("'", "''");

		sql = "SELECT * FROM login as l, users as u WHERE  l.iduser = u.id AND l.visibility= true AND  " + 
				column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Login login = new Login();

			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setLevel((String) result.get("level"));

			Integer i = (Integer) result.get("enable");
			if(i.equals(0)) {
				login.setEnabled(false);
			}
			else {
				login.setEnabled(true);
			}

			login.setVisibility(true);

			user.setId((Long) result.get("iduser"));
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			logins.add(login);
		}
		return logins;
	}

	@Override
	public int delete(Login entity) {

		Login login = entity;

		int res = 0;

		String sql = String.format("UPDATE login l SET l.visibility = false WHERE id = %d", login.getId());

		res = dbr.delete(sql);

		sql = String.format("UPDATE users u SET u.visibility = false WHERE id = %d", login.getUser().getId());

		res = res + dbr.delete(sql);

		return res;
	}

	@Override
	public List<Login> getbyFilter(String condition) {

		List<Login> logins = new ArrayList <Login> ();

		String sql = "SELECT * FROM login as l, users as u WHERE l.visibility=true AND"
				+ " l.iduser = u.id AND " + condition;
		System.out.println(sql);
		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Login login = new Login();

			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setLevel((String) result.get("level"));

			Integer i = (Integer) result.get("enable");
			if(i.equals(0)) {
				login.setEnabled(false);
			}
			else {
				login.setEnabled(true);
			}

			login.setVisibility(true);

			user.setId((Long) result.get("iduser") );
			user.setSurname((String) result.get("surname"));
			user.setName  ((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);

			login.setUser(user);

			logins.add(login);
		}
		return logins;	
	}

	public Login checkLogin(Login entity) {

		List<Map<String, Object>> results;
		Login login = entity;
		User user = entity.getUser();

		//user sbagliato
		login.setLevel("Utente sconosciuto");
		user.setVisibility(false);

		// uno dei due errato
		String sql = ("SELECT * FROM login as l, users as u WHERE l.visibility = true AND "
				+ " l.enable = true AND l.iduser = u.id AND l.username ='" + login.getUsername() + "'");

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			//username giusto pass sbagliata

			user.setId((Long) result.get("iduser") );
			user.setSurname((String) result.get("surname"));
			user.setName((String) result.get("name"));
			user.setAddress((String) result.get("address"));
			user.setCap((String) result.get("cap"));
			user.setCity((String) result.get("city"));
			user.setRole((String) result.get("role"));			
			user.setVisibility(true);
			
			login.setLevel("Password errata");

			sql = "SELECT *  FROM login as l, users as u WHERE l.visibility = true AND l.enable = true"
					+ " AND l.iduser = u.id AND l.username ='" + login.getUsername() +
					"' AND l.password ='" + login.getPassword() + "'";

			results = dbr.select(sql);	

			for(Map<String, Object> result2 : results) {

				// user giusto pass giusta

				login.setId((Long) result2.get("id"));
				login.setLevel((String) result2.get("level"));
				user.setVisibility(true);
			}
		}
		return login;
	}

	@Override
	public List<Login> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
