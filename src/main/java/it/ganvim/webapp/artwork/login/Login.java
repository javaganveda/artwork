package it.ganvim.webapp.artwork.login;

import javax.validation.constraints.Size;

import it.ganvim.webapp.artwork.core.BaseDomain;
import it.ganvim.webapp.artwork.user.User;

public class Login extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	// ATTRIBBUTI	

	@Size(min = 8, message = "La lunghezza deve essere almeno 8 caratteri")
	private String username;

	@Size(min = 3, message = "La lunghezza deve essere almeno 3 caratteri")
	private String password;

	@Size(min = 1, message = "Campo obbligatorio")
	private String level; 
	
	private Boolean enabled;

	private User user;      // OGGETTO PER RICHIAMARE LA CLASSE "USER"

	// METODI
	public Login () {

		this.setUsername("");
		this.setPassword("");
		this.setLevel("");
		this.setEnabled(true);
		this.user = new User();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() 
	{
		return this.username + " " + this.password + " " + this.level + " " + this.enabled + " ";

	}
}