package it.ganvim.webapp.artwork.login;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.validation.constraints.Size;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class LoginController extends Controller<Long, Login> {

	private static final long serialVersionUID = 1L;

	@Size(min = 1, message = "Campo obbligatorio")
	private String username="";

	@Size(min = 1, message = "Campo obbligatorio")
	private String password="";

	private Login login;
	
	@Inject
	private SingletonDate sd = new SingletonDate();

	@Inject
	private Repository<Long, Login> userRepository;

	@PostConstruct
	public void init() {
		login = new Login();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
	public Repository<Long, Login> getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(Repository<Long, Login> userRepository) {
		this.userRepository = userRepository;
	}
	
	public SingletonDate getSd() {
		return sd;
	}

	public void setSd(SingletonDate sd) {
		this.sd = sd;
	}

	public void checkLogin() {

		Login ilogin = new Login();

		String page = "";

		ilogin.setUsername(username);	
		ilogin.setPassword(password);

		LoginRepository lr = new LoginRepository();

		this.login = lr.checkLogin(ilogin);

		if(this.login.getLevel().equals("Admin")) {
			// Apri la pagina admin
			page = "admin/joinquestionario.xhtml";
		}
		else {
			if(this.login.getLevel().equals("User")) {
				// Apri la pagina consulente
				page = "user/statistiche.xhtml";
			}	
		}

		if(this.login.getLevel().equals("Admin") || this.login.getLevel().equals("User")) {
			
			sd.setIdLogin(this.login.getId());
			sd.setLevel(this.login.getLevel());
			sd.setUsername(this.login.getUsername());
			this.redirect(page);
		}
	}
}