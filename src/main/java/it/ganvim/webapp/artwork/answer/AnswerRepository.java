package it.ganvim.webapp.artwork.answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class AnswerRepository implements Repository<Long,Answer> {

	DBRepository dbr = new DBRepository();

	public int add(Answer entity) {

		Answer answer = entity;

		int res = 0;

		String sql = String.format("INSERT INTO answers (idquestion, iddomainanswer,"
				+ " visibility) VALUES (%d, %d, %b) ", 
				answer.getIdQuestion(), answer.getIdDomainAnswer(), true);

		res = dbr.insert(sql);

		return res; 				
	}

	public List<Answer> getAll() {

		List<Answer> answers = new ArrayList <Answer> ();

		String sql = String.format("SELECT * FROM answers as a, domainanswers as d WHERE a.iddomainanswer = d.id AND a.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Answer answer = new Answer();

			answer.setId((Long) result.get("id"));
			answer.setIdQuestion((Long) result.get("idquestion"));
			answer.setIdDomainAnswer((Long) result.get("iddomainanswer"));
			answer.setName((String) result.get("name")); 
			answer.setValue((Double) result.get("value"));
			answer.setVisibility(true);

			answers.add(answer);
		}
		return answers;	
	}

	@Override
	public int update(Answer entity) {

		Answer answer = entity;

		int res = 0;

		String sql = String.format("UPDATE answers a  SET idquestion = %d,"
				+ " iddomainanswer = %d, visibility = %b WHERE a.id = %d",
				answer.getIdQuestion(), answer.getIdDomainAnswer(), true,answer.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public Answer getbyId(Long id) {

		Answer answer = new Answer();

		String sql = String.format("SELECT * FROM answers as a , domainanswers as d WHERE a.iddomainanswer = d.id AND a.visibility = true"
				+ " AND a.id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			answer.setId((Long) result.get("id"));
			answer.setIdQuestion((Long) result.get("idquestion"));
			answer.setIdDomainAnswer((Long) result.get("iddomainanswer"));
			answer.setName((String) result.get("name")); 
			answer.setValue((Double) result.get("value"));
			answer.setVisibility(true);
		}
		return answer;	
	}

	@Override
	public List<Answer> getbyColumn(String column, String value) {

		List<Answer> answers = new ArrayList <Answer> ();

		String sql = "SELECT * FROM answers as a, domainanswers as d WHERE a.iddomainanswer = d.id AND  a.visibility = true AND a."
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Answer answer = new Answer();

			answer.setId((Long) result.get("id"));
			answer.setIdQuestion((Long) result.get("idquestion"));
			answer.setIdDomainAnswer((Long) result.get("iddomainanswer"));
			answer.setName((String) result.get("name")); 
			answer.setValue((Double) result.get("value"));
			answer.setVisibility(true);

			answers.add(answer);
		}
		return answers;
	}

	@Override
	public int delete(Answer entity) {

		Answer answer = entity;

		int res = 0;

		String sql = String.format("UPDATE answers a SET a.visibility = false WHERE"
				+ " id = %d", answer.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<Answer> getbyFilter(String condition) {

		List<Answer> answers = new ArrayList <Answer> ();

		String sql = "SELECT * FROM answers as a, domainanswers as d WHERE a.iddomainanswer = d.id AND a.visibility = true AND "
				+ condition + " ORDER BY d.value";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Answer answer = new Answer();

			answer.setId((Long) result.get("id"));
			answer.setIdQuestion((Long) result.get("idquestion"));
			answer.setIdDomainAnswer((Long) result.get("iddomainanswer"));
			answer.setName((String) result.get("name")); 
			answer.setValue((Double) result.get("value"));
			answer.setVisibility(true);

			answers.add(answer);
		}
		return answers;
	}

	@Override
	public List<Answer> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}