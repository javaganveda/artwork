package it.ganvim.webapp.artwork.answer;

import java.util.ArrayList;
import java.util.List;

import it.ganvim.webapp.artwork.choice.Choice;
import it.ganvim.webapp.artwork.core.BaseDomain;

public class Answer extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private Long IdQuestion;
	private Long IdDomainAnswer;
	private String name;
	private Double value;
	
	private List<Choice> choice;

	public Answer() {

		this.setIdDomainAnswer(0L);
		this.setIdQuestion(0L);
		this.setName("");
		this.setValue(0.0);
		this.setChoice(new ArrayList<Choice>());
	}

	public Long getIdDomainAnswer() {
		return IdDomainAnswer;
	}

	public void setIdDomainAnswer(Long idDomainAnswer) {
		IdDomainAnswer = idDomainAnswer;
	}

	public List<Choice> getChoice() {
		return choice;
	}

	public void setChoice(List<Choice> choice) {
		this.choice = choice;
	}

	public Long getIdQuestion() {
		return IdQuestion;
	}

	public void setIdQuestion(Long idQuestion) {
		IdQuestion = idQuestion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
}
