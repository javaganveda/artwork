package it.ganvim.webapp.artwork.choice;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class Choice extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private Long customer;
	private Long idanswer;
	private boolean selected;

	public Choice () {

		this.setCustomer(0L);
		this.setIdanswer(0L);
		this.setSelected(false);
	}

	public Long getCustomer() {
		return customer;
	}
	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public Long getIdanswer() {
		return idanswer;
	}

	public void setIdanswer(Long idanswer) {
		this.idanswer = idanswer;
	}

	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
