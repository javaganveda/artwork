package it.ganvim.webapp.artwork.choice;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class ChoiceRepository implements Repository<Long,Choice> {

	DBRepository dbr = new DBRepository();

	public int add(Choice entity) {

		Choice choice = entity;

		int res = 0;

		String sql = String.format("INSERT INTO choices (customer, idanswer, "
				+ "visibility) VALUES (%d, %d, %b)", 
				choice.getCustomer(), choice.getIdanswer(), true);

		res = dbr.insert(sql);

		return res; 
	}				

	public List<Choice> getAll() {

		List<Choice> choices = new ArrayList <Choice> ();

		String sql = String.format("SELECT * FROM choices as c WHERE c.visibility=true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Choice choice = new Choice();

			choice.setId((Long) result.get("id"));
			choice.setCustomer((Long) result.get("customer"));
			choice.setIdanswer((Long) result.get("idanswer"));
			
			/*
			Integer i = (Integer) result.get("selected");		
			if(i.equals(0)) {
				choice.setSelected(false);
			}
			else {
				choice.setSelected(true);
			}
			*/	
			
			choice.setVisibility(true);

			choices.add(choice);
		}
		return choices;	
	}

	@Override
	public int update(Choice entity) {

		Choice choice = entity;

		int res = 0;

		String sql = String.format("UPDATE choices as c  SET customer = %d, "
				+ "idanswer = %d, visibility = %b WHERE c.id = %d",
				choice.getCustomer(), choice.getIdanswer(),
				choice.isVisible(), choice.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public Choice getbyId(Long id) {

		Choice choice= new Choice();

		String sql = String.format("SELECT * FROM choices as c WHERE "
				+ "c.visibility = true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			choice.setId((Long) result.get("id"));
			choice.setCustomer((Long) result.get("customer"));
			choice.setIdanswer((Long) result.get("idanswer"));
			
			/*
			Integer i = (Integer) result.get("selected");
			if(i.equals(0)) {
				choice.setSelected(false);
			}
			else {
				choice.setSelected(true);
			}
			*/
			
			choice.setVisibility(true);
		}
		return choice;	
	}

	@Override
	public List<Choice> getbyColumn(String column, String value) {

		List<Choice> choices = new ArrayList <Choice> ();

		String sql = "SELECT * FROM choices as c WHERE  c.visibility=true AND c." + column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Choice choice = new Choice();

			choice.setId((Long) result.get("id"));
			choice.setCustomer((Long) result.get("customer"));
			choice.setIdanswer((Long) result.get("idanswer"));
			
			/*
			Integer i = (Integer) result.get("selected");
			if(i.equals(0)) {
				choice.setSelected(false);
			}
			else {
				choice.setSelected(true);
			}
			*/
			
			choice.setVisibility(true);

			choices.add(choice);
		}
		return choices;
	}

	@Override
	public int delete(Choice entity) {

		Choice choice = entity;

		int res = 0;

		String sql = String.format("UPDATE choices u SET u.visibility = false WHERE id = %d", choice.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<Choice> getbyFilter(String condition) {

		List<Choice> choices = new ArrayList <Choice> ();

		String sql = "SELECT * FROM choices as c WHERE  c.visibility = true AND "
		+ condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Choice choice = new Choice();

			choice.setId((Long) result.get("id"));
			choice.setCustomer((Long) result.get("customer"));
			choice.setIdanswer((Long) result.get("idanswer"));
			
			/*
			Integer i = (Integer) result.get("selected");	
			if(i.equals(0)) {
				choice.setSelected(false);
			}
			else {
				choice.setSelected(true);
			}
			*/
			
			choice.setVisibility(true);

			choices.add(choice);
		}
		return choices;
	}

	@Override
	public List<Choice> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}