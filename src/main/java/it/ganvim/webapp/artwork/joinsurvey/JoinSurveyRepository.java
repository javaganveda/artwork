package it.ganvim.webapp.artwork.joinsurvey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class JoinSurveyRepository implements Repository<Long,JoinSurvey> {

	DBRepository dbr = new DBRepository();

	public int add(JoinSurvey entity) {
		
		JoinSurvey joinSurvey = entity;

		int res = 0;

		List<JoinSurvey> joinSurveys = new ArrayList<JoinSurvey>();

		joinSurvey.setJKey(joinSurvey.getJKey().replace ("'", "''"));

		String condition = "jkey = '" + joinSurvey.getJKey() + "'" ;

		joinSurveys = this.getbyFilter(condition);

		if (joinSurveys.size() > 0) {
			return 1;
		}
		else {

			String sql = String.format("INSERT INTO joinsurveys (jkey, idsurvey, visibility)"
					+ " VALUES ('%s',%d, %b)", 
					joinSurvey.getJKey(), joinSurvey.getIdSurvey(), true);

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<JoinSurvey> getAll() {

		List<JoinSurvey> surveys = new ArrayList <JoinSurvey> ();

		String sql = String.format("SELECT * FROM joinsurveys as j WHERE j.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			JoinSurvey joinSurvey = new JoinSurvey();

			joinSurvey.setId((Long) result.get("id"));
			joinSurvey.setJKey((String) result.get("jkey"));
			joinSurvey.setIdSurvey((Long) result.get("idsurvey"));
			joinSurvey.setVisibility(true);

			surveys.add(joinSurvey);
		}
		return surveys;	
	}

	@Override
	public int update(JoinSurvey entity) {

		JoinSurvey joinSurvey = entity;

		int res = 0;

		joinSurvey.setJKey(joinSurvey.getJKey().replace ("'", "''"));

		String sql = "UPDATE joinsurveys j  SET jkey = '" + joinSurvey.getJKey() + "', idsurvey = "+
		joinSurvey.getIdSurvey() + ", visibility = "
		+ joinSurvey.isVisible() + " WHERE jkey = '" + joinSurvey.getJKey() + "'";

		res = dbr.update(sql);

		return res;
	}

	@Override
	public JoinSurvey getbyId(Long id) {

		JoinSurvey joinSurvey = new JoinSurvey();

		String sql = "SELECT * FROM joinsurveys as j WHERE j.visibility = true AND id = " + id;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			joinSurvey.setId((Long) result.get("id"));
			joinSurvey.setJKey((String) result.get("jkey"));
			joinSurvey.setIdSurvey((Long) result.get("idsurvey"));
			joinSurvey.setVisibility(true);

			joinSurvey.setVisibility(true);
		}
		return joinSurvey;	
	}

	@Override
	public List<JoinSurvey> getbyColumn(String column, String value) {

		List<JoinSurvey> joinSurveys = new ArrayList <JoinSurvey> ();
		
		column = column.replace ("'", "''");
		value = value.replace ("'", "''");
		
		String sql = "SELECT * FROM joinsurveys as j WHERE  j.visibility = true AND j." 
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			JoinSurvey joinSurvey = new JoinSurvey();

			joinSurvey.setId((Long) result.get("id"));
			joinSurvey.setJKey((String) result.get("jkey"));
			joinSurvey.setIdSurvey((Long) result.get("idsurvey"));
			joinSurvey.setVisibility(true);

			joinSurveys.add(joinSurvey);
		}
		return joinSurveys;
	}

	@Override
	public int delete(JoinSurvey entity) {

		JoinSurvey joinsurvey = entity;

		int res = 0;

		String sql = String.format("UPDATE joinsurveys j SET j.visibility = false WHERE id = %d",
				joinsurvey.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<JoinSurvey> getbyFilter(String condition) {

		List<JoinSurvey> joinSurveys = new ArrayList <JoinSurvey> ();

		String sql = "SELECT * FROM joinsurveys WHERE visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			JoinSurvey joinSurvey = new JoinSurvey();

			joinSurvey.setId((Long) result.get("id"));
			joinSurvey.setJKey((String) result.get("jkey"));
			joinSurvey.setIdSurvey((Long) result.get("idsurvey"));
			joinSurvey.setVisibility(true);

			joinSurveys.add(joinSurvey);
		}
		return joinSurveys;
	}

	@Override
	public List<JoinSurvey> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isModified(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}