package it.ganvim.webapp.artwork.joinsurvey;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class JoinSurvey extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String jkey;

	private Long idSurvey;

	public JoinSurvey () {

		this.setJKey  ("");
		this.setIdSurvey(0L);
	}
	
	public String getJKey() {
		return jkey;
	}

	public void setJKey(String jkey) {
		this.jkey = jkey;
	}

	public long getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(Long idSurvey) {
		this.idSurvey = idSurvey;
	}
}