package it.ganvim.webapp.artwork.joinsurvey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.survey.Survey;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class JoinSurveyController extends Controller<Long, JoinSurvey> {

	private static final long serialVersionUID = 1L;
	//STATIC
	public final static String ADULTS = "Adulti";
	public final static String KIDS = "Ragazzi";
	public final static String GROUPS = "Gruppi";
	public final static String EN = "Inglese";
	public final static String FR = "Francese";
	public final static String DE = "Tedesco";
	public final static String ES = "Spagnolo";
	
	private Map<String, Long> joinSurveyKey = new HashMap<String, Long>();
	private List<JoinSurvey> joinSurveys = new ArrayList<JoinSurvey>();
	
	private List<Survey> surveysAdults = new ArrayList<Survey>();
	private List<Survey> surveysKids = new ArrayList<Survey>();
	private List<Survey> surveysGroups = new ArrayList<Survey>();
	private List<Survey> surveysEng = new ArrayList<Survey>();
	private List<Survey> surveysFra = new ArrayList<Survey>();
	private List<Survey> surveysDeu = new ArrayList<Survey>();
	private List<Survey> surveysEsp = new ArrayList<Survey>();
	
	private Long notJoin;
		
	@Inject
	private Repository<Long, JoinSurvey> joinSurveyRepository;

	@Inject
	private Repository<Long, Survey> surveyRepository;
	
	
	public void init() {

		//id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;

		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		joinSurveyKey.put(JoinSurveyController.ADULTS,0L);
		joinSurveyKey.put(JoinSurveyController.KIDS,0L);
		joinSurveyKey.put(JoinSurveyController.GROUPS,0L);
		joinSurveyKey.put(JoinSurveyController.EN,0L);
		joinSurveyKey.put(JoinSurveyController.FR,0L);
		joinSurveyKey.put(JoinSurveyController.DE,0L);
		joinSurveyKey.put(JoinSurveyController.ES,0L);
		
		this.setNotJoin(0L);
			
		this.load();
	}
	
	public void load() {
		this.setJoinSurveys(joinSurveyRepository.getAll());
		
		for(int i = 0; i < joinSurveys.size(); i++) {
			joinSurveyKey.put(joinSurveys.get(i).getJKey(),joinSurveys.get(i).getIdSurvey());	
		}
		
		this.setSurveysAdults(surveyRepository.getbyColumn("typology", "Adulti"));
		this.setSurveysGroups(surveyRepository.getbyColumn("typology", "Gruppi"));
		this.setSurveysKids(surveyRepository.getbyColumn("typology", "Ragazzi"));
		this.setSurveysEng(surveyRepository.getbyColumn("typology", "Inglese"));
		this.setSurveysFra(surveyRepository.getbyColumn("typology", "Francese"));
		this.setSurveysDeu(surveyRepository.getbyColumn("typology", "Tedesco"));
		this.setSurveysEsp(surveyRepository.getbyColumn("typology", "Spagnolo"));
	}
	
	public void addUpdate() {
		
			int res = 0;

			JoinSurvey js = new JoinSurvey();
			js.setJKey(JoinSurveyController.ADULTS);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.ADULTS));
	
			res = joinSurveyRepository.update(js);
			
			js.setJKey(JoinSurveyController.KIDS);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.KIDS));
	
			res = res + joinSurveyRepository.update(js);
			js.setJKey(JoinSurveyController.GROUPS);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.GROUPS));
	
			res = res + joinSurveyRepository.update(js);
			
			js.setJKey(JoinSurveyController.EN);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.EN));
	
			res = res + joinSurveyRepository.update(js);
			
			js.setJKey(JoinSurveyController.FR);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.FR));
	
			res = res + joinSurveyRepository.update(js);
			
			js.setJKey(JoinSurveyController.DE);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.DE));
	
			res = res + joinSurveyRepository.update(js);

			js.setJKey(JoinSurveyController.ES);
			js.setIdSurvey(joinSurveyKey.get(JoinSurveyController.ES));
	
			res = res + joinSurveyRepository.update(js);
			
			if(res == 0) {	
				this.getSd().setMessage("Modifica avvenuta correttamente");	
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == -1) {		
				this.getSd().setMessage("Errore nella modifica");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}
			
			this.redirect("joinquestionario.xhtml");
	}
	
	public Map<String, Long> getJoinSurveyKey() {
		return joinSurveyKey;
	}

	public void setJoinSurveyKey(Map<String, Long> joinSurveyKey) {
		this.joinSurveyKey = joinSurveyKey;
	}

	public List<JoinSurvey> getJoinSurveys() {
		return joinSurveys;
	}

	public void setJoinSurveys(List<JoinSurvey> joinSurveys) {
		this.joinSurveys = joinSurveys;
	}

	public Repository<Long, Survey> getSurveyRepository() {
		return surveyRepository;
	}

	public void setSurveyRepository(Repository<Long, Survey> surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	public Long getNotJoin() {
		return notJoin;
	}

	public void setNotJoin(Long notJoin) {
		this.notJoin = notJoin;
	}

	public List<Survey> getSurveysAdults() {
		return surveysAdults;
	}

	public void setSurveysAdults(List<Survey> surveysAdults) {
		this.surveysAdults = surveysAdults;
	}

	public List<Survey> getSurveysKids() {
		return surveysKids;
	}

	public void setSurveysKids(List<Survey> surveysKids) {
		this.surveysKids = surveysKids;
	}

	public List<Survey> getSurveysGroups() {
		return surveysGroups;
	}

	public void setSurveysGroups(List<Survey> surveysGroups) {
		this.surveysGroups = surveysGroups;
	}

	public List<Survey> getSurveysFra() {
		return surveysFra;
	}

	public void setSurveysFra(List<Survey> surveysFra) {
		this.surveysFra = surveysFra;
	}

	public List<Survey> getSurveysEng() {
		return surveysEng;
	}

	public void setSurveysEng(List<Survey> surveysEng) {
		this.surveysEng = surveysEng;
	}

	public List<Survey> getSurveysDeu() {
		return surveysDeu;
	}

	public void setSurveysDeu(List<Survey> surveysDeu) {
		this.surveysDeu = surveysDeu;
	}

	public List<Survey> getSurveysEsp() {
		return surveysEsp;
	}

	public void setSurveysEsp(List<Survey> surveysEsp) {
		this.surveysEsp = surveysEsp;
	}
}
