package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.answer.Answer;
import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswer;
import it.ganvim.webapp.artwork.domainquestion.DomainQuestion;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestion;
import it.ganvim.webapp.artwork.question.Question;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class SurveyCombineController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;

	private Survey survey;

	private List<DomainAnswer> Answers = new ArrayList<DomainAnswer>();
	private List<DomainQuestion> Questions = new ArrayList<DomainQuestion>();
	private Long NewAnswer = 0L;
	private Long NewQuestion = 0L;
	private String  NewGroupQuestion = "";

	@Inject
	private Repository<Long, Survey> surveyRepository;
	@Inject
	private Repository<Long, GroupQuestion> gqRepository;
	@Inject
	private Repository<Long, Question> qRepository;
	@Inject
	private Repository<Long, Answer> aRepository;
	@Inject
	private Repository<Long, DomainQuestion> dqRepository;
	@Inject
	private Repository<Long, DomainAnswer> daRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
		//setId(this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 1L);
		
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.survey = new Survey();
		this.list();
	}

	public void list() {
		this.setSurvey(surveyRepository.getbyId(this.getSd().getIdSurvey()));
		this.setQuestions(dqRepository.getAll());
		this.setAnswers(daRepository.getAll());
	}

	public void deleteGq(Long idGq) {
		
		int res = 0;

		GroupQuestion gq = new GroupQuestion();
		gq.setId(idGq);
		
		res = gqRepository.delete(gq);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);	
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		// Redirigere la pagina
		this.redirect("associadomande.xhtml");
	}
	
	public void updateGq(Long idGq, String gqname) {
		
		int res = 0;

		GroupQuestion gq = new GroupQuestion();
		gq.setId(idGq);
		gq.setName(gqname);
		gq.setIdsurvey(survey.getId());
		
		res = gqRepository.update(gq);
		
		if(res == 0) {			
			this.getSd().setMessage("Modifica avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);	
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella modifica");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		// Redirigere la pagina
		this.redirect("associadomande.xhtml");
	}
	
public void deleteQ(Long idQ) {
		
		int res = 0;

		Question q = new Question();
		q.setId(idQ);
		
		res = qRepository.delete(q);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);	
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		
		// Redirigere la pagina
		this.redirect("associadomande.xhtml");
	}
	
	public void addQ(Long idGQ, Long idDQ) {
		
		int res = 0;

		Question q = new Question();
		q.setIdGroupQuestion(idGQ);
		q.setIdDomainQuestion(idDQ);
		
		if(idGQ!=0 && idDQ!=0) {

			res = qRepository.add(q);

			if(res == 0) {				
				this.getSd().setMessage("Domanda aggiunta correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);	
			}

			if(res == -1) {				
				this.getSd().setMessage("Errore nella modifica");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
			}

			// Redirigere la pagina
			this.redirect("associadomande.xhtml");
		}
	}
	
public void deleteA(Long idA) {
		
		int res = 0;

		Answer a = new Answer();
		a.setId(idA);
		
		res = aRepository.delete(a);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);	
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		// Redirigere la pagina
		this.redirect("associadomande.xhtml");
	}
	
public void addA(Long idQ, Long idDA ) {

	int res = 0;

	Answer a = new Answer();
	a.setIdQuestion(idQ);
	a.setIdDomainAnswer(idDA);

	if(idQ!=0 && idDA!=0) {

		res = aRepository.add(a);

		if(res == 0) {			
			this.getSd().setMessage("Risposta aggiunta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);	
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella modifica");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		// Redirigere la pagina
		this.redirect("associadomande.xhtml");
	}
}
	
	public void addGQ(Long idS, String nameGQ ) {
		
		int res = 0;

		GroupQuestion gq = new GroupQuestion();
		gq.setName(nameGQ);;
		gq.setIdsurvey(idS);;
		
		if(idS!=0) {
			res = gqRepository.add(gq);

			if(res == 0) {			
				this.getSd().setMessage("Gruppo domanda aggiunta correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);	
			}

			if(res == -1) {				
				this.getSd().setMessage("Errore nella modifica");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
			}

			// Redirigere la pagina
			this.redirect("associadomande.xhtml");
		}
	}
	
	public void onPanelCollapsed(int panel) {	
		
		boolean[] panels = new boolean[100];
		
		for(int i=0; i<100; i++) {
			panels[i] = true;		
		}
		panels[panel] = false;
		
		this.getSd().setPanelCollapsed(panels);
		//this.getSd().setPanelCollapsed("Form:j_idt61:" + panel + ":gq");
		//this.redirect("associadomande.xhtml");
	}
	
	public boolean modified(Long idsurvey) {

		boolean res = surveyRepository.isModified(idsurvey);

		return res;
	}
	
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public Repository<Long, Question> getqRepository() {
		return qRepository;
	}

	public void setqRepository(Repository<Long, Question> qRepository) {
		this.qRepository = qRepository;
	}

	public Repository<Long, Answer> getaRepository() {
		return aRepository;
	}

	public void setaRepository(Repository<Long, Answer> aRepository) {
		this.aRepository = aRepository;
	}

	public List<DomainAnswer> getAnswers() {
		return Answers;
	}

	public void setAnswers(List<DomainAnswer> Answers) {
		this.Answers = Answers;
	}

	public List<DomainQuestion> getQuestions() {
		return Questions;
	}

	public void setQuestions(List<DomainQuestion> Questions) {
		this.Questions = Questions;
	}

	public Repository<Long, DomainQuestion> getDqRepository() {
		return dqRepository;
	}

	public void setDqRepository(Repository<Long, DomainQuestion> dqRepository) {
		this.dqRepository = dqRepository;
	}

	public Repository<Long, DomainAnswer> getDaRepository() {
		return daRepository;
	}

	public void setDaRepository(Repository<Long, DomainAnswer> daRepository) {
		this.daRepository = daRepository;
	}

	public Long getNewAnswer() {
		return NewAnswer;
	}

	public void setNewAnswer(Long newAnswer) {
		NewAnswer = newAnswer;
	}

	public Long getNewQuestion() {
		return NewQuestion;
	}

	public void setNewQuestion(Long newQuestion) {
		NewQuestion = newQuestion;
	}

	public String getNewGroupQuestion() {
		return NewGroupQuestion;
	}

	public void setNewGroupQuestion(String newGroupQuestion) {
		NewGroupQuestion = newGroupQuestion;
	}
}