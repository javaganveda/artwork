package it.ganvim.webapp.artwork.survey;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class SurveyAddUpdateController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;

	private Survey survey;

	@Inject
	private Repository<Long, Survey> surveyRepository;

	public void init() {

		//id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;

		if (this.getSd().getIdSurvey().equals(0L)) {		
			survey = new Survey();
			survey.setId(0L);
		}
		else
		{
			survey = surveyRepository.getbyId(this.getSd().getIdSurvey());
		}
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public void addUpdate() {

		int res = 0;

		if(survey.getId().equals(0L)) {

			res = surveyRepository.add(survey);
			if(res == 0) {	
				this.getSd().setMessage("Inserimento avvenuto correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);	
				
				//Abilita il pulsante Associa domande dopo aver letto l'id asseganto
				this.getSd().setIdSurvey(surveyRepository.getbyColumn("name", survey.getName()).get(0).getId());
				this.redirect("salvaquestionario.xhtml");
			}

			if(res == 1) {				
				this.getSd().setMessage("Questionario gi� presente in archivio");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);	
			}

			if(res == -1) {		
				this.getSd().setMessage("Errore nell'inserimento");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
			}	
		}
		else{

			res = surveyRepository.update(survey);

			if(res == 0) {			
				this.getSd().setMessage("Modifica avvenuta correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);	
			}

			if(res == 1) {				
				this.getSd().setMessage("Questionario gi� presente in archivio");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);	
			}

			if(res == -1) {					
				this.getSd().setMessage("Errore nella modifica");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
			}
		}
	}
}
