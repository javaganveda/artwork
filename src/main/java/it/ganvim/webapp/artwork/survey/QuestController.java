package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.choice.Choice;
import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswer;
import it.ganvim.webapp.artwork.joinsurvey.JoinSurvey;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class QuestController extends Controller<Long, DomainAnswer> {

	private static final long serialVersionUID = 1L;
	
	private Long idAdults = 0L;
	private Long idGroups = 0L;
	private Long idKids = 0L;
	private Long idEng = 0L;
	private Long idFra = 0L;
	private Long idDeu = 0L;
	private Long idEsp = 0L;
	private String language = "";
	
	private JoinSurvey js = new JoinSurvey();

	@Inject
	private Repository<Long, JoinSurvey> joinSurveyRepository;

	public void init() {
		
		this.setLanguage(this.getParameters().containsKey("l") ? getParameters().get("l") : "");
	
		if(this.getLanguage().equals("")) {
			this.list();
		}
		else {
			this.open(language);
		}				
	}

	public void list() {
		
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Adulti").get(0));
		this.setIdAdults(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Gruppi").get(0));
		this.setIdGroups(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Ragazzi").get(0));
		this.setIdKids(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Inglese").get(0));
		this.setIdEng(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Francese").get(0));
		this.setIdFra(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Tedesco").get(0));
		this.setIdDeu(this.getJs().getIdSurvey());
		this.setJs(joinSurveyRepository.getbyColumn("jkey", "Spagnolo").get(0));
		this.setIdEsp(this.getJs().getIdSurvey());
	}

	public void openSurvey(String survey) {
		
		//List<Choice> newallChoices = new ArrayList<Choice>();
		this.getSd().setAllChoices(new ArrayList<Choice>());
		this.getSd().setChoices(new HashMap<Long, String>());
		
		this.getSd().setCustomer(0L);
		this.getSd().setnQuest(1);
		
		switch (survey) {
			case "adulti":
				{
					this.getSd().setIdSurvey(this.getIdAdults());
					this.getSd().setLanguage("ita");
					this.getSd().setTitle("QUESTIONARIO | ADULTI SINGOLI");
					this.getSd().setFlag("ita");
					this.getSd().setQuestion("Domanda");
					this.getSd().setQuitKey("Abbandona");
					this.getSd().setNextKey("Avanti");
					break;
				}
			case "gruppi":
			{
				this.getSd().setIdSurvey(this.getIdGroups());
				this.getSd().setLanguage("ita");
				this.getSd().setTitle("QUESTIONARIO | GRUPPI ORGANIZZATI");
				this.getSd().setFlag("ita");
				this.getSd().setQuestion("Domanda");
				this.getSd().setQuitKey("Abbandona");
				this.getSd().setNextKey("Avanti");
				break;
			}
			case "ragazzi":
			{
				this.getSd().setIdSurvey(this.getIdKids());
				this.getSd().setLanguage("ita");
				this.getSd().setTitle("QUESTIONARIO | RAGAZZI");
				this.getSd().setFlag("ita");
				this.getSd().setQuestion("Domanda");
				this.getSd().setQuitKey("Abbandona");
				this.getSd().setNextKey("Avanti");
				break;
			}
			case "eng":
			{
				this.getSd().setIdSurvey(this.getIdEng());
				this.getSd().setLanguage("eng");
				this.getSd().setTitle("SURVAY");
				this.getSd().setFlag("uk");
				this.getSd().setQuestion("Question");
				this.getSd().setQuitKey("Quit");
				this.getSd().setNextKey("Next");
				break;
			}
			case "fra":
			{
				this.getSd().setIdSurvey(this.getIdFra());
				this.getSd().setLanguage("fra");
				this.getSd().setTitle("QUESTIONNAIRE");
				this.getSd().setFlag("fra");
				this.getSd().setQuestion("Question");
				this.getSd().setQuitKey("Laisser");
				this.getSd().setNextKey("Suivant");
				break;
			}
			case "deu":
			{
				this.getSd().setIdSurvey(this.getIdDeu());
				this.getSd().setLanguage("deu");
				this.getSd().setTitle("FRAGEBOGEN");
				this.getSd().setFlag("ger");
				this.getSd().setQuestion("Nachfrage");
				this.getSd().setQuitKey("Laub");
				this.getSd().setNextKey("Vorw�rts");
				break;
			}
			case "esp":
			{
				this.getSd().setIdSurvey(this.getIdEsp());
				this.getSd().setLanguage("esp");
				this.getSd().setTitle("CUESTIONARIO");
				this.getSd().setFlag("spa");
				this.getSd().setQuestion("Demanda");
				this.getSd().setQuitKey("Hojas");
				this.getSd().setNextKey("Siguiente");
				break;
			}
		}
		
		this.redirect("answer.xhtml");	
	}
	
	public void open(String language) {
		
		String page = "";
		
		switch (language) {
			case "ita":
			{
				this.getSd().setMessage("");
				this.getSd().setLanguage("ita");
				this.getSd().setTitle("QUESTIONARIO");
				this.getSd().setFlag("ita");
				page = "quest_ita.xhtml";
				break;
			}

			case "eng":
			{
				this.getSd().setMessage("");
				this.getSd().setLanguage("eng");
				this.getSd().setTitle("SURVAY");
				this.getSd().setFlag("uk");
				page = "quest_eng.xhtml";
				break;
			}
			case "fra":
			{
				this.getSd().setMessage("");
				this.getSd().setLanguage("fra");
				this.getSd().setTitle("QUESTIONNAIRE");
				this.getSd().setFlag("fra");
				page = "quest_fra.xhtml";
				break;
			}
			case "deu":
			{
				this.getSd().setMessage("");
				this.getSd().setLanguage("deu");
				this.getSd().setTitle("FRAGEBOGEN");
				this.getSd().setFlag("ger");
				page = "quest_deu.xhtml";
				break;
			}
			case "esp":
			{
				this.getSd().setMessage("");
				this.getSd().setLanguage("esp");
				this.getSd().setTitle("CUESTIONARIO");
				this.getSd().setFlag("spa");
				page = "quest_esp.xhtml";
				break;
			}
		}		
		this.redirect(page);
	}
	

	public JoinSurvey getJs() {
		return js;
	}

	public void setJs(JoinSurvey js) {
		this.js = js;
	}

	public Long getIdAdults() {
		return idAdults;
	}

	public void setIdAdults(Long idAdults) {
		this.idAdults = idAdults;
	}

	public Long getIdGroups() {
		return idGroups;
	}

	public void setIdGroups(Long idGroups) {
		this.idGroups = idGroups;
	}

	public Long getIdKids() {
		return idKids;
	}

	public void setIdKids(Long idKids) {
		this.idKids = idKids;
	}

	public Long getIdEng() {
		return idEng;
	}

	public void setIdEng(Long idEng) {
		this.idEng = idEng;
	}

	public Long getIdFra() {
		return idFra;
	}

	public void setIdFra(Long idFra) {
		this.idFra = idFra;
	}

	public Long getIdDeu() {
		return idDeu;
	}

	public void setIdDeu(Long idDeu) {
		this.idDeu = idDeu;
	}

	public Long getIdEsp() {
		return idEsp;
	}

	public void setIdEsp(Long idEsp) {
		this.idEsp = idEsp;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}