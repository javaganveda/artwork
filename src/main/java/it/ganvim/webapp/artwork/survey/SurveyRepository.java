package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.answer.Answer;
import it.ganvim.webapp.artwork.answer.AnswerRepository;
import it.ganvim.webapp.artwork.choice.Choice;
import it.ganvim.webapp.artwork.choice.ChoiceRepository;
import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestion;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestionRepository;
import it.ganvim.webapp.artwork.question.Question;
import it.ganvim.webapp.artwork.question.QuestionRepository;

@Dependent
@Singleton
public class SurveyRepository implements Repository<Long,Survey> {

	DBRepository dbr = new DBRepository();

	public int add(Survey entity) {

		Survey survey = entity;

		int res = 0;

		List<Survey> surveys = new ArrayList<Survey>();

		survey.setName(survey.getName().replace ("'", "''"));

		String condition = " name = '" + survey.getName() + "'" ;

		surveys = this.getbyFilter(condition);

		if (surveys.size() > 0) {
			return 1;
		}
		else {

			survey.setTypology(survey.getTypology().replace ("'", "''"));
			survey.setDescription(survey.getDescription().replace ("'", "''"));

			String sql = String.format("INSERT INTO surveys (name, typology, description,"
					+ " visibility) VALUES ('%s','%s','%s', %b)", 
					survey.getName(), survey.getTypology(), survey.getDescription(), true);

			res = dbr.insert(sql);

			return res; 
		}				
	}

	public List<Survey> getAll() {

		List<Survey> surveys = new ArrayList <Survey> ();

		String sql = String.format("SELECT * FROM surveys as s WHERE s.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Survey survey = new Survey();

			survey.setId((Long) result.get("id"));
			survey.setName((String) result.get("name"));
			survey.setTypology((String) result.get("typology"));
			survey.setDescription ((String) result.get("description"));
			survey.setVisibility(true);

			surveys.add(survey);
		}
		return surveys;	
	}
	
	public List<Survey> getAllStats() {

		List<Survey> surveys = new ArrayList <Survey> ();

		String sql = String.format("SELECT * FROM surveys as s WHERE s.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {
			surveys.add(this.getForStats((Long) result.get("id")));
		}
		
		return surveys;	
	}

	@Override
	public int update(Survey entity) {

		Survey survey = entity;

		int res = 0;

		survey.setName(survey.getName().replace ("'", "''"));
		survey.setTypology(survey.getTypology().replace ("'", "''"));
		survey.setDescription(survey.getDescription().replace ("'", "''"));

		String sql = "UPDATE surveys s  SET name = '" + survey.getName() + "', typology = '"+
		survey.getTypology() + "', description = '" + survey.getDescription() + "', visibility = "
		+ survey.isVisible() + " WHERE id= " + survey.getId();

		res = dbr.update(sql);

		return res;
	}

	@Override
	public Survey getbyId(Long id) {

		Survey survey = new Survey();

		String sql = "SELECT * FROM surveys as s WHERE s.visibility = true AND id = " + id;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			survey.setId((Long) result.get("id"));
			survey.setName((String) result.get("name"));
			survey.setTypology((String) result.get("typology"));
			survey.setDescription ((String) result.get("description"));
			survey.setVisibility(true);
		}

		GroupQuestionRepository gqr = new GroupQuestionRepository();
		List<GroupQuestion> gq = new ArrayList<GroupQuestion>();

		gq = gqr.getbyFilter("idsurvey = " + survey.getId());

		for(int i = 0; i<gq.size();i++) {

			QuestionRepository qr = new QuestionRepository();
			List<Question> q = new ArrayList<Question>();		
			q = qr.getbyFilter("idgroupquestion = " + gq.get(i).getId());
			gq.get(i).setQuestions(q);

			for(int j = 0; j<q.size();j++) {
				AnswerRepository ar = new AnswerRepository();
				List<Answer> a = new ArrayList<Answer>();
				a = ar.getbyFilter("idquestion = " + q.get(j).getId());
				q.get(j).setAnswers(a);	
			}		
		}
		survey.setGroupQuestions(gq);
		
		return survey;	
	}

	@Override
	public List<Survey> getbyColumn(String column, String value) {

		List<Survey> surveys = new ArrayList <Survey> ();

		String sql = "SELECT * FROM surveys as s WHERE  s.visibility = true AND s." 
				+ column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Survey survey = new Survey();

			survey.setId((Long) result.get("id"));
			survey.setName((String) result.get("name"));
			survey.setTypology((String) result.get("typology"));
			survey.setDescription ((String) result.get("description"));
			survey.setVisibility(true);

			surveys.add(survey);
		}
		return surveys;
	}

	@Override
	public int delete(Survey entity) {

		Survey survey = entity;

		int res = 0;

		String sql = String.format("UPDATE surveys s SET s.visibility = false WHERE id = %d",
				survey.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<Survey> getbyFilter(String condition) {

		List<Survey> surveys = new ArrayList <Survey> ();

		String sql = "SELECT * FROM surveys as s WHERE  s.visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			Survey survey = new Survey();

			survey.setId((Long) result.get("id"));
			survey.setName((String) result.get("name"));
			survey.setTypology((String) result.get("typology"));
			survey.setDescription ((String) result.get("description"));
			survey.setVisibility(true);

			surveys.add(survey);
		}
		return surveys;
	}
	
	public Survey getForStats(Long id) {

		Survey survey = new Survey();

		String sql = "SELECT * FROM surveys as s WHERE s.visibility = true AND id = " + id;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			survey.setId((Long) result.get("id"));
			survey.setName((String) result.get("name"));
			survey.setTypology((String) result.get("typology"));
			survey.setDescription ((String) result.get("description"));
			survey.setVisibility(true);
		}

		GroupQuestionRepository gqr = new GroupQuestionRepository();
		List<GroupQuestion> gq = new ArrayList<GroupQuestion>();

		gq = gqr.getbyFilter("idsurvey = " + survey.getId());

		for(int i = 0; i < gq.size();i++) {

			QuestionRepository qr = new QuestionRepository();
			List<Question> q = new ArrayList<Question>();		
			q = qr.getbyFilter("idgroupquestion = " + gq.get(i).getId());
			gq.get(i).setQuestions(q);

			for(int j = 0; j < q.size();j++) {
				AnswerRepository ar = new AnswerRepository();
				List<Answer> a = new ArrayList<Answer>();
				a = ar.getbyFilter("idquestion = " + q.get(j).getId());
				q.get(j).setAnswers(a);	

				for(int k = 0; k < a.size();k++) {
					ChoiceRepository cr = new ChoiceRepository();
					List<Choice> c = new ArrayList<Choice>();
					c = cr.getbyFilter("idanswer = " + a.get(k).getId());
					a.get(k).setChoice(c);	
				}
			}		
		}
		survey.setGroupQuestions(gq);
		
		//calcola le risposte e le statistiche
		Long nq;
		double average;
		int av=0;
		double averagePercentage;
		int avp=0;
		double max;
		String trend;
		int nTrend;
		String dataChart;
			
		for(int i = 0; i <  survey.getGroupQuestions().size();i++) {
			for(int j = 0; j <  survey.getGroupQuestions().get(i).getQuestions().size();j++) {
				
				dataChart = "";
				nTrend = 0;
				trend = "";
				average = 0.0;
				averagePercentage = 0.0;
				nq = 0L;
				max=1L;
				
				for(int k = 0; k <  survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().size();k++) {

					if(max < survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getValue()) {
						max = survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getValue();
					}
					
					nq = nq + survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size();

					average = average + (survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getValue()*survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size());

					if (nTrend<=survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size()){
						if (nTrend==survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size()){
							trend = trend + "; " + survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getName();
							nTrend = survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size();
						}
						else {
							trend = survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getName();
							nTrend = survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size();

						}
					}

					dataChart = dataChart + "['" + survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getName() +
							"'," + survey.getGroupQuestions().get(i).getQuestions().get(j).getAnswers().get(k).getChoice().size() +
							"],";			
				}

				average = average / nq;
				averagePercentage = average * 100 / max;
				
				// Arrotondamento alle due cifre decimali
				
				av= (int)  (average * 100);
				avp= (int)  (averagePercentage * 100);
				
				average = (double) av;
				averagePercentage = (double) avp;
				average = average  / 100;
				averagePercentage = averagePercentage  / 100;
				
				survey.getGroupQuestions().get(i).getQuestions().get(j).setDataChart(dataChart);
				survey.getGroupQuestions().get(i).getQuestions().get(j).setTrend(trend);
				survey.getGroupQuestions().get(i).getQuestions().get(j).setAverage(average);
				survey.getGroupQuestions().get(i).getQuestions().get(j).setAveragePercentage(averagePercentage);
				survey.setnQueries(nq);
			}
		}
		return survey;	
	}

	@Override
	public boolean isModified(Long id) {
		
		boolean modified = false;

		String sql = String.format("SELECT * FROM surveys AS s, groupquestions AS gq, questions AS q,"
				+ " answers AS a, choices AS c " + 
				"WHERE s.id = gq.idsurvey AND gq.id = q.idgroupquestion AND q.id = a.idquestion AND"
				+ " a.id = c.idanswer AND s.id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		if(results.size() > 0) {
			modified = false;
		}
		else { 
			modified = true;
			
		}

		return modified;	
	}
}