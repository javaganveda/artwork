package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.joinsurvey.JoinSurvey;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class SurveyListController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;

	private List<Survey> surveys;
	private Long idSurveyDel = 0L;

	@Inject
	private Repository<Long, Survey> surveyRepository;
	
	@Inject
	private Repository<Long, JoinSurvey> jsr;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.setSurveys(new ArrayList<Survey>());
		this.list();
	}

	public void list() {
		this.setSurveys(surveyRepository.getAll());
	}

	public void delete() {
		
		int res = 0;

		Survey survey = new Survey();
		
		survey.setId(this.getIdSurveyDel());
		DBRepository dbr = new DBRepository();
		
		List<JoinSurvey> js = new ArrayList<JoinSurvey>();

		js = jsr.getbyFilter("idsurvey = " + this.getIdSurveyDel());
		
		for(int i = 0; i< js.size(); i++) {
			String sql = "UPDATE joinsurveys   SET idsurvey = 0 WHERE id = " +  js.get(i).getId();
			res = dbr.update(sql);
		}
		
		res = surveyRepository.delete(survey);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);	
		}

		// Redirigere la pagina
		this.redirect("questionario.xhtml");
	}
	
	public void candidatedIdSurveyDel(Long idSurvey) {
		this.setIdSurveyDel(idSurvey);	
	}
	
	public void addupdate(Long idSurvey) {	
		this.getSd().setIdSurvey(idSurvey);
		this.changePage("salvaquestionario.xhtml");	
	}
	
	public List<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(List<Survey> surveys) {
		this.surveys = surveys;
	}

	public Long getIdSurveyDel() {
		return idSurveyDel;
	}

	public void setIdSurveyDel(Long idSurveyDel) {
		this.idSurveyDel = idSurveyDel;
	}	
}