package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.choice.Choice;
import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.question.Question;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class SurveyDispenseController extends Controller<Long, Survey> {

	private static final long serialVersionUID = 1L;
	
	private Survey survey;
	private String gq = "";
	private List<Question> questions = new ArrayList<Question>();
	private int totQuests = 0;
	private double step = 0;
	private String lstep = "";
	private String inlineAnswer = "block";
	
	@Inject
	private Repository<Long, Survey> surveyRepository;
	@Inject
	private Repository<Long, Choice> choiceRepository;

	public void init() {
		
		//setMessage(this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "");
		//setId(this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L);
		//setnQuest(this.getParameters().containsKey("NQ") ? Integer.parseInt(getParameters().get("NQ")) : 0);
		
		if(this.getSd().getCustomer().equals(0L)) {
			for(int i=0; i<choiceRepository.getAll().size(); i++){
				this.getSd().setCustomer(choiceRepository.getAll().get(i).getCustomer());
			}
			this.getSd().setCustomer(this.getSd().getCustomer()+1);
		}
		
		this.survey = new Survey();
		this.list();
		
		this.totQuests = this.survey.getGroupQuestions().size();
		this.step = (double) (this.getSd().getnQuest() - 1) / (double) this.totQuests * 100;
		this.setLstep("" + this.step);
		
		this.setGq(this.survey.getGroupQuestions().get(this.getSd().getnQuest() - 1).getName());
		this.setQuestions(this.survey.getGroupQuestions().get(this.getSd().getnQuest() - 1).getQuestions());
		
		if (this.getGq().equals("")) {		
			this.setInlineAnswer("block");	
		}
		else {
			this.setInlineAnswer("inline");
		}
		
		
		if(this.getSd().getAllChoices().size() == 0) {

			for(int k=0; k<this.survey.getGroupQuestions().size(); k++) {
				for(int i=0; i < this.survey.getGroupQuestions().get(k).getQuestions().size(); i++){
					for(int j=0; j<this.survey.getGroupQuestions().get(k).getQuestions().get(i).getAnswers().size(); j++){

						Choice ch = new Choice();
						ch.setCustomer(this.getSd().getCustomer());
						ch.setIdanswer(this.survey.getGroupQuestions().get(k).getQuestions().get(i).getAnswers().get(j).getId());
						ch.setSelected(false);	
						this.getSd().getAllChoices().add(ch);
					}
				}
			}

		}
					
	}

	public void list() {
		this.setSurvey(surveyRepository.getbyId(this.getSd().getIdSurvey()));
	}
	
	public void next() {

		for(int K = 0; K < this.getSd().getAllChoices().size(); K++) {

			if(this.getSd().getChoices().containsValue("" + this.getSd().getAllChoices().get(K).getIdanswer())) {
				this.getSd().getAllChoices().get(K).setSelected(true);
			}
		}
		
		this.getSd().setnQuest(this.getSd().getnQuest() + 1);
		String page = "";
		
		Map<Long, String> newchoices  = new HashMap<Long, String>();		
		this.getSd().setChoices(newchoices);
		
		if(this.getSd().getnQuest() > this.totQuests) {
			
			//Scrive le risposte sul data-base
			for(int i = 0; i< this.getSd().getAllChoices().size(); i ++) {
				
				if(this.getSd().getAllChoices().get(i).getSelected() == true) {
					this.choiceRepository.add(this.getSd().getAllChoices().get(i));
				}
			}
			
			switch (this.getSd().getLanguage()) {
				case "ita":
				{
					page= "quest_fine.xhtml";
					break;
				}
				case "eng":
				{
					page= "quest_end.xhtml";
					break;
				}
				case "fra":
				{
					page= "quest_fin.xhtml";
					break;
				}
				case "deu":
				{
					page= "quest_ende.xhtml";
					break;
				}
				case "esp":
					{
						page= "quest_final.xhtml";
						break;
					}
				}
		}
		else {
			page = "answer.xhtml";
		}
		
		this.redirect(page);
	}

	public String getGq() {
		return gq;
	}

	public void setGq(String gq) {
		this.gq = gq;
	}

	public String getLstep() {
		return lstep;
	}

	public void setLstep(String lstep) {
		this.lstep = lstep;
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public int getTotQuests() {
		return totQuests;
	}

	public void setTotQuests(int totQuests) {
		this.totQuests = totQuests;
	}

	public double getStep() {
		return step;
	}

	public void setStep(double step) {
		this.step = step;
	}


	public Repository<Long, Survey> getSurveyRepository() {
		return surveyRepository;
	}

	public void setSurveyRepository(Repository<Long, Survey> surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	public String getInlineAnswer() {
		return inlineAnswer;
	}

	public void setInlineAnswer(String inlineAnswer) {
		this.inlineAnswer = inlineAnswer;
	}
}