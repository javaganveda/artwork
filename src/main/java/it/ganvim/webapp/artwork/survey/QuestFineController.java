package it.ganvim.webapp.artwork.survey;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;
import it.ganvim.webapp.artwork.domainanswer.DomainAnswer;
import it.ganvim.webapp.artwork.email.Email;
import it.ganvim.webapp.artwork.suggest.Suggest;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class QuestFineController extends Controller<Long, DomainAnswer> {

	private static final long serialVersionUID = 1L;
	
	private Email eMail = new Email();
	private Suggest suggest = new Suggest();
 
	@Inject
	private Repository<Long, Email> emailRepository;
	
	@Inject
	private Repository<Long, Suggest> suggestRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	}
	
	public void end(String Language) {

		if(this.geteMail().getEmail().length()==0 || (this.geteMail().getEmail().contains("@") && this.geteMail().getEmail().contains("."))) {
			if(!this.geteMail().getEmail().equals("")) {

				emailRepository.add(eMail);
			}

			if(!this.getSuggest().getSuggest().equals("")) {

				suggestRepository.add(suggest);		
			}

			this.redirect("credits_" + Language + ".xhtml");

		}
		else {
			//errore E-mail
			switch(this.getSd().getLanguage()) {
			case "ita":
				this.getSd().setMessage("Formato E-Mail non valido");
				break;
			case "eng":
				this.getSd().setMessage("Invalid E-Mail format");
				break;
			case "fra":
				this.getSd().setMessage("Format E-Mail invalide");
				break;
			case "deu":
				this.getSd().setMessage("Ung�ltiges E-Mail format");
				break;
			case "esp":
				this.getSd().setMessage("Formato E-Mail inv�lido");
				break;
			default:
				this.getSd().setMessage("");
				break;						
			}
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
		}
	}


	public Email geteMail() {
		return eMail;
	}

	public void seteMail(Email eMail) {
		this.eMail = eMail;
	}

	public Suggest getSuggest() {
		return suggest;
	}

	public void setSuggest(Suggest suggest) {
		this.suggest = suggest;
	}
}