package it.ganvim.webapp.artwork.survey;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

import it.ganvim.webapp.artwork.core.BaseDomain;
import it.ganvim.webapp.artwork.groupquestion.GroupQuestion;

public class Survey extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	public final String TYPOLOGY = "typology";


	@Size(min = 1, message = "Campo obbligatorio")
	private String name;
	
	@Size(min = 1, message = "Campo obbligatorio")
	private String typology;
	
	private String description;
	private Long nQueries;

	private List<GroupQuestion> groupQuestions;

	public Survey () {

		this.setName  ("");
		this.setTypology("");
		this.setDescription("");
		this.setnQueries(0L);
		this.groupQuestions = new ArrayList<GroupQuestion>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<GroupQuestion> getGroupQuestions() {
		return groupQuestions;
	}

	public void setGroupQuestions(List<GroupQuestion> groupQuestions) {
		this.groupQuestions = groupQuestions;
	}

	public Long getnQueries() {
		return nQueries;
	}

	public void setnQueries(Long nQueries) {
		this.nQueries = nQueries;
	}
}