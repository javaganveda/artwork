package it.ganvim.webapp.artwork.domainanswer;

import it.ganvim.webapp.artwork.core.BaseDomain;

public class DomainAnswer extends BaseDomain<Long> {

	private static final long serialVersionUID = 1L;

	private String name;
	private Double value;

	public DomainAnswer () {

		this.setName("");
		this.setValue(0.0);	
	}	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}	
}