package it.ganvim.webapp.artwork.domainanswer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganvim.webapp.artwork.core.DBRepository;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.survey.Survey;

@Dependent
@Singleton
public class DomainAnswerRepository implements Repository<Long,DomainAnswer> {

	DBRepository dbr = new DBRepository();

	public int add(DomainAnswer entity) {

		DomainAnswer domainanswer = entity;

		int res = 0;

		domainanswer.setName(domainanswer.getName().replace ("'", "''"));	

		String sql = String.format(Locale.US, "INSERT INTO domainanswers (name, value, "
				+ "visibility) VALUES ('%s',%f, %b)", 
				domainanswer.getName(), domainanswer.getValue(),true);

		res = dbr.insert(sql);

		return res; 
	}				

	public List<DomainAnswer> getAll() {

		List<DomainAnswer> domainanswers = new ArrayList <DomainAnswer> ();

		String sql = String.format("SELECT * FROM domainanswers as d WHERE"
				+ " d.visibility = true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainAnswer domainanswer = new DomainAnswer();

			domainanswer.setId((Long) result.get("id"));
			domainanswer.setName((String) result.get("name"));
			domainanswer.setValue((Double) result.get("value"));
			domainanswer.setVisibility(true);

			domainanswers.add(domainanswer);
		}
		return domainanswers;	
	}

	@Override
	public int update(DomainAnswer entity) {

		DomainAnswer domainanswer = entity;

		int res = 0;

		domainanswer.setName(domainanswer.getName().replace ("'", "''"));

		String sql = "UPDATE domainanswers as d  SET name = '" 
				+ domainanswer.getName() + "', value = " + domainanswer.getValue()
				+ ", visibility = " + domainanswer.isVisible()
				+ " WHERE id = " + domainanswer.getId();
		
		res = dbr.update(sql);

		return res;
	}

	@Override
	public DomainAnswer getbyId(Long id) {

		DomainAnswer domainanswer = new DomainAnswer ();

		String sql = String.format("SELECT * FROM domainanswers as d WHERE "
				+ "d.visibility = true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			domainanswer.setId((Long) result.get("id"));
			domainanswer.setName((String) result.get("name"));
			domainanswer.setValue((Double) result.get("value"));
			domainanswer.setVisibility(true);

		}
		return domainanswer;	
	}

	@Override
	public List<DomainAnswer> getbyColumn(String column, String value) {

		List<DomainAnswer> domainanswers = new ArrayList <DomainAnswer> ();

		String sql = "SELECT * FROM domainanswers as d WHERE "
				+ " d.visibility = true AND d." + column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainAnswer domainanswer = new DomainAnswer();

			domainanswer.setId((Long) result.get("id"));
			domainanswer.setName((String) result.get("name"));
			domainanswer.setValue((Double) result.get("value"));
			domainanswer.setVisibility(true);

			domainanswers.add(domainanswer);
		}
		return domainanswers;
	}


	@Override
	public int delete(DomainAnswer entity) {

		DomainAnswer domainanswer = entity;

		int res = 0;

		String sql = String.format("UPDATE domainanswers as d SET "
				+ "d.visibility = false WHERE" + " id = %d", domainanswer.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<DomainAnswer> getbyFilter(String condition) {

		List<DomainAnswer> domainanswers = new ArrayList <DomainAnswer> ();

		String sql = "SELECT * FROM domainanswers as d WHERE"
				+ "  d.visibility = true AND " + condition;

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			DomainAnswer domainanswer = new DomainAnswer();

			domainanswer.setId((Long) result.get("id"));
			domainanswer.setName((String) result.get("name"));
			domainanswer.setValue((Double) result.get("value"));
			domainanswer.setVisibility(true);

			domainanswers.add(domainanswer);
		}
		return domainanswers;
	}

	@Override
	public List<DomainAnswer> getAllStats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey getForStats(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isModified(Long id) {
		
		boolean modified = false;

		String sql = String.format("SELECT * FROM surveys AS s, groupquestions AS gq, questions AS q,"
				+ "answers AS a, choices AS c WHERE s.id = gq.idsurvey AND gq.id = q.idgroupquestion"
				+ " AND q.id = a.idquestion AND a.id = c.idanswer AND s.id IN ( "
				+ "SELECT s.id FROM surveys AS s, groupquestions AS gq, questions AS q, answers AS a "
				+ "WHERE s.id = gq.idsurvey AND gq.id = q.idgroupquestion AND q.id = a.idquestion AND"
				+ " a.iddomainanswer = %d)", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		if(results.size() > 0) {
			modified = false;
		}
		else { 
			modified = true;
			
		}

		return modified;	
	}
}
