package it.ganvim.webapp.artwork.domainanswer;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.ganvim.webapp.artwork.core.Controller;
import it.ganvim.webapp.artwork.core.Repository;
import it.ganvim.webapp.artwork.core.SingletonDate;

@ManagedBean
//@ApplicationScoped
//@RequestScoped
@ViewScoped
public class DomainAnswerController extends Controller<Long, DomainAnswer> {

	private static final long serialVersionUID = 1L;

	private List<DomainAnswer> domainAnswers;
	
	private String newanswer = "";
	
	private Double newvalue = 0.0;

	@Inject
	private Repository<Long, DomainAnswer> domainAnswerRepository;

	public void init() {
		
		//message = this.getParameters().containsKey("Messaggio") ? getParameters().get("Messaggio") : "";
	
		if (this.getSd().getIdLogin().equals(0L)) {		
			//Ritorna al Login
			this.redirect("../login.xhtml");
		}
		
		this.domainAnswers = new ArrayList<DomainAnswer>();
		this.list();
	}

	public void list() {
		this.domainAnswers = domainAnswerRepository.getAll();
	}

	public void delete(Long idDomainAnswer) {
		
		int res = 0;

		DomainAnswer domainAnswer = new DomainAnswer();
		
		domainAnswer.setId(idDomainAnswer);
		
		res = domainAnswerRepository.delete(domainAnswer);
		
		if(res == 0) {			
			this.getSd().setMessage("Cancellazione avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella cancellazione");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}

		// Redirigere la pagina
		this.redirect("risposte.xhtml");
	}
	
	public void update(Long idDomainAnswer, String answer, Double value) {

		int res = 0;

		DomainAnswer domainAnswer = new DomainAnswer();
		
		domainAnswer.setId(idDomainAnswer);
		domainAnswer.setValue(value);		
		domainAnswer.setName(answer);

		res = domainAnswerRepository.update(domainAnswer);

		if(res == 0) {			
			this.getSd().setMessage("Modifica avvenuta correttamente");
			this.getSd().setMessSeverity(SingletonDate.MESSOK);
		}

		if(res == 1) {				
			this.getSd().setMessage("Risposta gi� presente in archivio");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
		}

		if(res == -1) {				
			this.getSd().setMessage("Errore nella modifica");
			this.getSd().setMessSeverity(SingletonDate.MESSERROR);
		}
		
		// Redirigere la pagina
		this.redirect("risposte.xhtml");
	}
	
	public void add(String newanswer, Double newvalue) {

		int res = 0;

		DomainAnswer domainAnswer = new DomainAnswer();

		domainAnswer.setName(newanswer);
		domainAnswer.setValue(newvalue);

		if(newanswer.equals("")) {
			this.getSd().setMessage("Campo nuova risposta vuoto");
			this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			// Redirigere la pagina
			this.redirect("risposte.xhtml");
		}
		else
		{
			res = domainAnswerRepository.add(domainAnswer);

			if(res == 0) {	
				this.getSd().setMessage("Inserimento avvenuto correttamente");
				this.getSd().setMessSeverity(SingletonDate.MESSOK);
			}

			if(res == 1) {			
				this.getSd().setMessage("Risposta gi� presente in archivio");
				this.getSd().setMessSeverity(SingletonDate.MESSWORN);
			}

			if(res == -1) {		
				this.getSd().setMessage("Errore nell'inserimento");
				this.getSd().setMessSeverity(SingletonDate.MESSERROR);
			}	

			// Redirigere la pagina
			this.redirect("risposte.xhtml");
		}
	}
	
	public boolean modified(Long iddomainAnswer) {

		boolean res = domainAnswerRepository.isModified(iddomainAnswer);

		return res;
	}

	public List<DomainAnswer> getDomainAnswers() {
		return domainAnswers;
	}

	public void setDomainAnswers(List<DomainAnswer> domainAnswers) {
		this.domainAnswers = domainAnswers;
	}

	public String getNewanswer() {
		return newanswer;
	}

	public void setNewanswer(String newanswer) {
		this.newanswer = newanswer;
	}

	public Double getNewvalue() {
		return newvalue;
	}

	public void setNewvalue(Double newvalue) {
		this.newvalue = newvalue;
	}	
}